﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;
using System.Data.SqlClient;

namespace Arconic
{
    public class DatabaseHandler
    {
        public MySqlConnection Connection { get; set; } = null;
        private MySqlTransaction transaction = null;

        public void CommitTransaction()
        {
            if (transaction == null) return;

            transaction.Commit();
        }

        public void Connect(string connectionString)
        {
            Disconnect();

            Connection = new MySqlConnection(connectionString);
            Connection.Open();
        }

        public void Query(string query)
        {
            MySqlCommand cmd = new MySqlCommand(query, Connection);
            cmd.BeginExecuteNonQuery();
            cmd.Dispose();
        }

        public void Disconnect()
        {
            if (Connection == null) return;

            if (Connection.State != ConnectionState.Closed) Connection.Close();
            Connection.Dispose();
            Connection = null;
        }

        public void RollbackTransaction()
        {
            if (transaction == null) return;

            transaction.Rollback();
        }

        public void StartTransaction()
        {
            RollbackTransaction();

            transaction = Connection.BeginTransaction();
        }
    }

    public class MSDatabaseHandler
    {
        public SqlConnection MSconnection { get; set; } = null;
        private SqlTransaction Mstransaction = null;
        public string MSSqlConnection { get; set; } = null;

        public void CommitTransaction()
        {
            if (Mstransaction == null) return;

            Mstransaction.Commit();
        }

        public void Connect(string connectionString)
        {
            Disconnect();

            MSconnection = new SqlConnection(connectionString);
            MSconnection.Open();
        }

        public void Query(string query)
        {
            SqlCommand cmd = new SqlCommand(query, MSconnection);
            cmd.CommandType = CommandType.Text;
            SqlDataReader msdr = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(msdr);
            //cmd.BeginExecuteNonQuery();
            Console.WriteLine(cmd.CommandText);
            cmd.Dispose();
        }

        public void Disconnect()
        {
            if (MSconnection == null) return;

            if (MSconnection.State != ConnectionState.Closed) MSconnection.Close();
            MSconnection.Dispose();
            MSconnection = null;
        }

        public void RollbackTransaction()
        {
            if (Mstransaction == null) return;

            Mstransaction.Rollback();
        }

        public void StartTransaction()
        {
            RollbackTransaction();

            Mstransaction = MSconnection.BeginTransaction();
        }
    }
}
