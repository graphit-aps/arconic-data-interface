﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Data.SqlClient;

namespace Arconic
{
    [Serializable]
    public class MSDataTable : DataTable
    {
        private string fieldLastUpdate = "last_update";
        private string fieldCreatedAt = "created_at";
        private string fieldTrackChanges = "c_temp_field";

        private string[] columnToDownload;
        private string where;

        private bool noDelete;

        private SqlDataAdapter sqlAdapter;
        private SqlConnection sqlConnection;

        // Database Primary keys
        private List<string> columns = new List<string>();
        private List<string> primaryKeys = new List<string>();

        public MSDataTable(SqlConnection connection, string name, bool noDelete = false, string[] columns = null, string where = null)
        {
            TableName = name;
            sqlConnection = connection;
            this.noDelete = noDelete;
            columnToDownload = columns;
            this.where = where;

            Download();
        }

        /// <summary>
        /// Download data from the specified table.
        /// </summary>
        /// <param name="SchemaOnly"></param>
        public void Download()
        {
            string columns = "*";
            string where = this.where ?? "0 = 0";

            if (columnToDownload != null)
            {
               columns = string.Join(", ", columnToDownload.Select(el => $"`{el}`"));
            }
            
            string select = string.Format($"SELECT {columns} FROM {TableName} WHERE {where}");

            // Create table schema in C# and download records
            sqlAdapter = new SqlDataAdapter(string.Format(select, TableName), sqlConnection);

            // Download all data
            sqlAdapter.FillSchema(this, SchemaType.Source);
            sqlAdapter.Fill(this);

            // Collect primary keys
            foreach (DataColumn col in PrimaryKey)
            {
                primaryKeys.Add(col.ColumnName);
            }

            // Collect columns
            foreach (DataColumn col in Columns)
            {
                this.columns.Add(col.ColumnName);
            }

            // Add virtual column for track changes
            DataColumn column = new DataColumn(fieldTrackChanges, typeof(sbyte));
            column.AllowDBNull = true;
            Columns.Add(column);
        }

        public void Upload()
        {
            // DELETE NOT USED ROWS
            if (!noDelete)
            {
                DataRow[] deletedRows = Select($"{fieldTrackChanges} is null");
                foreach (var row in deletedRows)
                    row.Delete();
            }

            // Remove helper column
            Columns.Remove(fieldTrackChanges);


            // UPLOAD
            var cb = new SqlCommandBuilder(sqlAdapter);
            cb.ConflictOption = ConflictOption.CompareRowVersion;
            sqlAdapter.UpdateBatchSize = 100;
            sqlAdapter.Update(this);

            // Free memory
            Clear();
            GC.Collect();
        }

        public int NumberOfRowsToBeDeleted()
        {
            return Select($"{fieldTrackChanges} is null").Length;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="values">Values Dictionary where key is the database field and the value is the database value</param>
        /// <param name="ifnew">database field name array where the value only valid if this is a new row</param>
        public void AddRecord(Dictionary<string, object> values, List<string> ifnew = null)
        {
            // Try to find it
            bool add = false;
            DataRow row = null;
                row = Rows.Find(GetPrimaryKeyValues(values));

                if (row == null)
                {
                    // Row not exists yet, create it
                    add = true;
                    row = NewRow();

                    row[fieldLastUpdate] = DateTime.Now;
                    row[fieldCreatedAt] = DateTime.Now;
                }

                row.BeginEdit();

                // Update values
                foreach (var kvp in values)
                {
                    if (ifnew == null || !ifnew.Contains(kvp.Key) || add)
                    {
                        row[kvp.Key] = kvp.Value ?? DBNull.Value;
                    }
                }

                // Set track flag
                row[fieldTrackChanges] = 1;

                row.EndEdit();

                if (add)
                    Rows.Add(row);

        }


        private object[] GetPrimaryKeyValues(Dictionary<string, object> values)
        {
            var vals = new List<object>();

            foreach (var pk in primaryKeys)
            {
                vals.Add(values[pk]);
            }

            return vals.ToArray();
        }
    }
}
