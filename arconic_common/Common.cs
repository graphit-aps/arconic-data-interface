﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Threading.Tasks;

namespace Arconic
{
    public static class Common
    {
        #region "Config"
        public static Config GetConfigCommon()
        {
            return GetConfig<Config>("common.json");
        }

        public static T GetConfig<T>(string configName)
        {
            var cfgLines = File.ReadAllLines(Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "/configs", configName), UTF8Encoding.UTF8);
            return JsonConvert.DeserializeObject<T>(String.Join("\r\n", cfgLines));
        }
        #endregion

        #region "Excel"
        public static DataSet ParseExcel(string filePath)
        {
            DataSet result = null;

            //using (var stream = File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            //{
            //    using (var reader = ExcelReaderFactory.CreateReader(stream))
            //    {
            //        // 2. Use the AsDataSet extension method
            //        result = reader.AsDataSet(new ExcelDataSetConfiguration()
            //        {

            //            // Gets or sets a value indicating whether to set the DataColumn.DataType 
            //            // property in a second pass.
            //            UseColumnDataType = true,

            //            // Gets or sets a callback to obtain configuration options for a DataTable. 
            //            ConfigureDataTable = (tableReader) => new ExcelDataTableConfiguration()
            //            {

            //                // Gets or sets a value indicating the prefix of generated column names.
            //                EmptyColumnNamePrefix = "Column",

            //                // Gets or sets a value indicating whether to use a row from the 
            //                // data as column names.
            //                UseHeaderRow = false,

            //                // Gets or sets a callback to determine which row is the header row. 
            //                // Only called when UseHeaderRow = true.
            //                ReadHeaderRow = (rowReader) => {
            //                    // F.ex skip the first row and use the 2nd row as column headers:
            //                    rowReader.Read();
            //                }
            //            }
            //        });
            //    }
            //}

            return result;
        }
        #endregion

        public static string GetMeta(MSDataTable tbl, string meta, string key)
        {
            // Get row from table
            var row = tbl.Rows.Find(new object[] { meta, key });

            // If found return with date
            if (row != null)
                return row["value"].ToString();

            return null;
        }

        public static DateTime? GetMetaDateTime(MSDataTable tbl, string meta, string key)
        {
            var result = GetMeta(tbl, meta, key);

            if (result == null)
                return null;

            return Convert.ToDateTime(result);
        }

        /// <summary>
        /// Sets the last update time in the database for data object
        /// </summary>
        /// <param name="slug"></param>
        /// <param name="lastUpdate"></param>
        public static void SetMeta(MSDataTable tbl, string meta, string key, object value)
        {
            // Add record
            tbl.AddRecord(new Dictionary<string, object>() { { "meta", meta }, { "key", key }, { "value", value } });
        }

        public static void SetMeta(MSDataTable tbl, string meta, string key, DateTime? value)
        {
            SetMeta(tbl, meta, key, value == null ? null : ((DateTime)value).ToString("yyyy-MM-dd HH:mm:ss"));
        }
    }
}
