﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arconic.DataUploader
{
    //Extension methods must be defined in a static class
    public static class Extension
    {
        public static double ToDouble(this object str)
        {
            return Convert.ToDouble(str, CultureInfo.InvariantCulture);
        }

        public static int? ToInt(this object str)
        {
            if (str == null || str.ToString().Trim() == "")
                return null;

            return Convert.ToInt32(str, CultureInfo.InvariantCulture);
        }

        public static DateTime? ToDateTime(this object str)
        {
            if (str == null)
                return null;

            var formatStrings = new string[] { "dd.MM.yyyy", "yyyy-MM-dd", "yyyyMMdd", "yyyyMMdd HH:mm:ss", "dd/MM/yyyy HH:mm:ss", "yyyy-MM-dd hh:mm:ss", "yyyy-MM-dd HH:mm:ss", "dd.MM.yyyy hh:mm:ss", "dd.MM.yyyy HH:mm:ss", "yyyy.MM.dd. HH:mm:ss", "yyyy.MM.dd. hh:mm:ss" };
            if (!DateTime.TryParseExact(str.ToString(), formatStrings, CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime date))
                return null;

            return date;
        }

        public static IEnumerable<String> SplitInParts(this String s, Int32 partLength)
        {
            if (s == null)
                throw new ArgumentNullException("s");
            if (partLength <= 0)
                throw new ArgumentException("Part length has to be positive.", "partLength");

            for (var i = 0; i < s.Length; i += partLength)
                yield return s.Substring(i, Math.Min(partLength, s.Length - i));
        }
    }
}
