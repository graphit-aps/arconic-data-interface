﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Arconic;
using System.Data;

namespace graphIT.Arconic.TestConnection
{
    public class TestConnection
    {
        public static string tesConnectionStr = "Server=localhost;Database=arconic_develop;Trusted_Connection=True;";
        private MSDatabaseHandler msDB = new MSDatabaseHandler();

        public TestConnection(string connStr)
        {
            string sqlConnection = connStr;
            msDB.Connect(sqlConnection);

            Console.WriteLine("Connection success!");

            SqlCommand cmd = new SqlCommand("SELECT @@VERSION AS 'SQL Server Version'",msDB.MSconnection);
            cmd.CommandType = CommandType.Text;
            SqlDataReader datareader;
            datareader = cmd.ExecuteReader();
            DataTable dt = new DataTable();

            dt.Load(datareader);
            string sqlVersion = dt.Rows[0][0].ToString();
            Console.WriteLine(string.Format("Connected SQL Version: \n {0}", sqlVersion));

            msDB.Disconnect();
            Console.WriteLine("Connection closed");
        }
    }
}
