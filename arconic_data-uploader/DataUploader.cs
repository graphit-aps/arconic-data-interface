﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using Oracle.ManagedDataAccess.Client;
using System.Data.SqlClient;

namespace Arconic.DataUploader
{
    public class DataUploader
    {
        private MSDatabaseHandler msdb = new MSDatabaseHandler();
        // SQL tábla a feltöltendő adatokat tartalmazó táblák információjával:
        private MSDataTable tableMeta;
        // Config mappából az adatbázis konfigurációk kiolvasása (Oracle, mySQL, Excel):
        private Config cfgCommon = Common.GetConfigCommon();
        //private OracleConnection oradb;

        public DataUploader()
        {
            string sqlConnection = cfgCommon.ConnectionStringMSSQL;
            msdb.Connect(sqlConnection);

            Console.WriteLine("Connection success!");

            SqlCommand cmd = new SqlCommand("SELECT @@VERSION AS 'SQL Server Version'", msdb.MSconnection);
            cmd.CommandType = CommandType.Text;
            SqlDataReader datareader;
            datareader = cmd.ExecuteReader();
            DataTable dt = new DataTable();

            dt.Load(datareader);
            string sqlVersion = dt.Rows[0][0].ToString();
            Console.WriteLine(string.Format("Connected SQL Version: \n {0}", sqlVersion));

            msdb.Disconnect();
            Console.WriteLine("Connection closed");

        }


        private void Upload(string slug, string query, string tableName, Func<DataRow, Dictionary<string, object>> mapping)
        {
            Console.WriteLine($"Updating {slug}");

            // Create db table
            var table = new MSDataTable(msdb.MSconnection, tableName);

            //Lekérdezés parancs az Oracleből:
            SqlCommand cmd = new SqlCommand(query, msdb.MSconnection);
            cmd.CommandType = CommandType.Text;
            //Lekérdezés (a dr-be töltjük be az aktuálisan lekérdezett adatot):
            SqlDataReader dr = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            // C# táblába betöltése az eredménynek:
            dt.Load(dr);

            // Ha nincs még a táblában az adott sor, akkor betöltjük
            foreach (DataRow dRow in dt.Rows)
            {
                var raw = mapping(dRow);
                if (raw == null)
                    continue;

                table.AddRecord(raw);
            }
            dt.Dispose();
            dr.Dispose();

            // Upload table
            table.Upload();
            table = null;

            SetLastUpdateMeta(slug, DateTime.Now);
        }

        #region "Utils"


        /// <summary>
        /// Gets the last update time from database for data object
        /// </summary>
        /// <param name="slug">data slug</param>
        /// <returns></returns>
        private DateTime? GetLastUploadMeta(string slug)
        {
            return Common.GetMetaDateTime(tableMeta, "Data Uploaded", slug);
        }

        private DateTime? GetLastUpdateMeta(string slug)
        {
            return Common.GetMetaDateTime(tableMeta, "Data Updated", slug);
        }

        /// <summary>
        /// Sets the last update time in the database for data object
        /// </summary>
        /// <param name="slug"></param>
        /// <param name="lastUpdate"></param>
        private void SetLastUploadMeta(string slug, DateTime lastUpdate)
        {
            Common.SetMeta(tableMeta, "Data Uploaded", slug, lastUpdate);
        }

        private void SetLastUpdateMeta(string slug, DateTime lastUpdate)
        {
            Common.SetMeta(tableMeta, "Data Updated", slug, lastUpdate);
        }

        #endregion
    }
}
