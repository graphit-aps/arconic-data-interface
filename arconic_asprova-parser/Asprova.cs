﻿using System.Collections.Generic;
using System.Diagnostics;
using graphIT.Asprova;
using System.Data.SqlClient;

namespace graphIT.Arconic.AsprovaParser
{
    public class Asprova : AsBase
    {
        public MasterTable masters = null;
        public ItemTable items = null;
        public OrderTable orders = null;
        public ResultTable results = null;

        public Asprova(ref SqlConnection conn) : base(conn) { }

        public void Run(bool legacy)
        {
            orders = new OrderTable(this);
            tables.Add("order", orders);

            results = new ResultTable(this, legacy);
            tables.Add("result", results);

            Parse();
        }
    }
}
