﻿using graphIT.Asprova;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using static graphIT.Asprova.AsConst.Order;
using static graphIT.Asprova.AsConst;
using static graphIT.Asprova.AsConst.Item;

namespace graphIT.Arconic.AsprovaParser
{
    public class OrderTable : AsTable<Order>
    {

        #region Used strings
        readonly string updateSalesOrder = "Updating Sales Orders";
        readonly string updateInventoryOrder = "Updating Inventory Orders";
        readonly string itemString = "item";
        readonly string inventoryString = "inventory";
        readonly string gacCodeString = "gacCode";
        readonly string customerordersString = "customerorders";
        readonly string WipToSalesString = "wipToSales";
        readonly string item_code = "item_code";
        readonly string order_code = "order_code";
        readonly string order_status = "order_status";
        readonly string due_date = "due_date";
        readonly string ship_date = "ship_date";
        readonly string order_number = "order_number";
        readonly string open_qty = "open_qty";
        readonly string ordered_qty = "ordered_qty";
        readonly string customer_code = "customer_code";
        readonly string customer_name = "customer_name";
        readonly string forged_item_code = "forged_item_code";
        readonly string machined_item_code = "machined_item_code";
        readonly string pack_type = "pack_type";
        readonly string plan_prod_date = "plan_prod_date";
        readonly string inventory_qty = "inventory_qty";
        readonly string inventory_palette = "inventory_palette";
        readonly string inventory_status = "inventory_status";
        readonly string inventory_qa_status = "inventory_qa_status";
        readonly string inventory_type = "inventory_type";
        readonly string locationString = "location";
        readonly string inventory_id = "inventory_id";
        readonly string inventoryStatus = "inventoryStatus";
        readonly string inventoryQAStatus = "inventoryQAStatus";
        readonly string inventoryStatusCode = "inventoryStatusCode";
        readonly string inventoryStatusDesc = "inventoryStatusDesc";
        readonly string inventoryQAStatusCode = "inventoryQAStatusCode";
        readonly string inventoryQAStatusDesc = "inventoryQAStatusDesc";
        readonly string gac_codeString = "gac_code";
        readonly string box = "pack_box";
        readonly string gaz = "GAZ";
        readonly string gax = "GAX";
        readonly string missingItemString = "Hiányzó mesteradat";
        readonly string missingInventoryItem = "Hiányzó tétel a tételtáblában";
        readonly string wipId = "wip_id";
        readonly string wip = "WIP";
        readonly string timestamp = "timestamp";
        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="asprova">Parent Asprova object</param>
        public OrderTable(AsBase asprova) : base(asprova)
        {
            databaseTableName = "asp_order";

            name = "OrderTable";

            #region Sources
            asprova.AddSource(customerordersString, @"
                        SELECT id 'row_id', 
                               timestamp 'timestamp',
                               ORDER_NUMBER 'order_number',
                               LINE_NUMBER 'line_number',
                               CONCAT(CAST(CAST(ORDER_NUMBER as int) as varchar), '_', LINE_NUMBER) 'order_code',
                               ORDER_STATUS 'order_status',
                               ORDER_TYPE 'order_TYPE',
                               CUSTOMER_NUMBER 'customer_code',
                               CUSTOMER_NAME 'customer_name',
                               LOCATION 'location',
                               PO_NUMBER 'po_number',
                               ITEM_NUMBER 'item_code',
                               FORGED_DIE_NUMBER 'forged_item_code',
                               MACHINED_DIE_NUMBER 'machined_item_code',
                               SCHEDULE_SHIP_DATE 'ship_date',
                               OPEN_QUANTITY 'open_qty', 
                               ORDERED_QUANTITY 'ordered_qty', 
                               PACK_TYPE 'pack_type',
                               BOX 'pack_box',
                               PLAN_PRODUCTION_DATE 'plan_prod_date'
                       FROM awpe_ora_prod_demand
                       WHERE awpe_ora_prod_demand.timestamp = (SELECT MAX(awpe_ora_prod_demand.timestamp) FROM awpe_ora_prod_demand)
                             AND ORDER_TYPE != 'FORECAST'");

            asprova.AddSource(inventoryString, @"
                        SELECT 
                            timestamp 'timestamp',
							id_type 'inventory_type', 
	                        item_nr 'item_code',
	                        sum(qty) 'inventory_qty',
	                        status 'inventory_status',
	                        qa_status 'inventory_qa_status',
	                        palette 'inventory_palette',
                            pack_code 'gac_code',
                            wip_id 'wip_id'
	                    FROM awpe_inventory
	                    WHERE awpe_inventory.timestamp = (SELECT MAX(awpe_inventory.timestamp) FROM awpe_inventory)
	                    GROUP BY item_nr,id_type,status,qa_status,palette,pack_code,wip_id,timestamp");

            // Ez volt a verzió a készletek GAC kódja nélkül
            //asprova.AddSource(inventoryString, @"
            //            SELECT 
            //                id_type 'inventory_type', 
	           //             item_nr 'item_code',
	           //             sum(qty) 'inventory_qty',
	           //             status 'inventory_status',
	           //             qa_status 'inventory_qa_status',
	           //             palette 'inventory_palette'
	           //         FROM awpe_inventory
	           //         WHERE awpe_inventory.timestamp = (SELECT MAX(awpe_inventory.timestamp) FROM awpe_inventory)
	           //         GROUP BY item_nr,id_type,status,qa_status,palette");

            asprova.AddSource(itemString, @"
                         SELECT item_code,
                                warning
                         FROM asp_item WHERE error is null");

            asprova.AddSource(gacCodeString, @"
                         SELECT packing_code 'gacCode',
                                item_code,
                                customer_code,
                                location,
                                palette_type 'pack_type'
                         FROM pack_type");

            asprova.AddSource(inventoryStatus, @"
                         SELECT code 'inventoryStatusCode',
                                hu 'inventoryStatusDesc'
                         FROM awpe_wheel_status_codes
                         WHERE valid != 0");

            asprova.AddSource(inventoryQAStatus, @"
                         SELECT code 'inventoryQAStatusCode',
                                hu 'inventoryQAStatusDesc'
                         FROM awpe_wheel_qualification_codes
                         WHERE valid != 0");

            asprova.AddSource(WipToSalesString, @"
                     SELECT CONCAT(CAST(CAST(ORDER_NUMBER as int) as varchar), '_', LINE_NUMBER) 'order_code',
                         ORDER_NUMBER 'order_number',
                         LINE_NUMBER 'line_number',
                         WIP 'WIP',
                        START_QUANTITY 'start_qty',
                        QUANTITY_COMPLETED 'qty_completed',
                        STATUS 'status'
                        FROM[ASPROVA].[dbo].[awpe_ora_prod_demand_wips]");


            #endregion

            DownloadTargetDataSchema();
        }

        /// <summary>
        /// Generate Items
        /// </summary>
        public override void Generate()
        {
            if (Generated) return;
            Generated = true;

            GenerateInventoryOrders();

            GenerateSalesOrders();
        }

        public override void Save()
        {
            foreach (Order order in Values)
            {
                if (order.Type != OrderType.Manufacturing  && order.Type != OrderType.Sales)
                    continue;

                if (order.Item == null)
                    order.Disabled = true;
            }

            base.Save();
        }


        private bool IsValidSqlDateTime(string dateTime)
        {
            if (dateTime == null || dateTime == "")
            {
                return true;
            }
            else
            {
                DateTime? actDate = Convert.ToDateTime(dateTime);

                DateTime min = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
                DateTime max = (DateTime)System.Data.SqlTypes.SqlDateTime.MaxValue;

                if (min > actDate.Value || max < actDate.Value)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        /// <summary>
        /// Generates the Customer sales orders from erp database
        /// </summary>
        private void GenerateSalesOrders()
        {
            Console.WriteLine(updateSalesOrder);
            DataTable salessource = asprova.sources[customerordersString];
            DataTable items = asprova.sources[itemString];

            int numOfNotFoundItemInSO = 0;
            List<string> listOfNotFoundItems = new List<string>();
            List<string> EnteredSalesOrders = new List<string>();

            foreach (DataRow row in salessource.Rows)
            {
                if (row[order_status].ToString().ToUpper()== "ENTERED")
                {
                    EnteredSalesOrders.Add(row[order_code].ToString());
                    continue;
                }

                


                DateTime actualPlanProdDate = new DateTime();
                try
                {
                    // Check: order' item exists in Asprova item table (stored in SQL db)
                    string selectString = String.Format("{0}='{1}'", item_code, row[item_code].ToString());

                    if (row[item_code].ToString().ToUpper().Contains(gax) || row[item_code].ToString().ToUpper().Contains(gaz)) // Ha gax vagy gaz, akkor ne vegye át
                    {
                        continue;
                    }

                    if (!IsValidSqlDateTime(row[plan_prod_date].ToString())|| !IsValidSqlDateTime(row[ship_date].ToString()))
                    {
                        Console.WriteLine("A következő Sales Order hibás dátummal rendelkezik, ezért az Asprova nem veszi át:\n "+"   Order code: " + row[order_code].ToString() +
                            "\n    Plan prod date: " + row[plan_prod_date].ToString() + "\n    Scheduled ship date: " + row[ship_date].ToString()+"\n");
                        continue;
                    }

                        string actualItemCode = row[item_code].ToString();
                    string actualCustomer = row[customer_code].ToString();
                    string actualLocation = row[locationString].ToString();
                    string actualShipInstr = row[pack_type].ToString();
                    string actualGacCode = FindGacCode(actualItemCode, actualCustomer, actualLocation, actualShipInstr);

                    if (row[plan_prod_date] != DBNull.Value)
                    {
                        actualPlanProdDate = row.Field<DateTime>(plan_prod_date).AddHours(6);
                    }
                    else { }

                    var actualOrder = GenerateOrderObject(
                        code: row[order_code].ToString(),
                        erpCode: row[order_number].ToString(),
                        itemCode: actualItemCode,
                        erpType: OrderType.Sales,
                        let: row.Field<DateTime>(ship_date),
                        qtyOpen: Convert.ToInt32(row[open_qty]),
                        qtyOrd: Convert.ToInt32(row[ordered_qty]),
                        customerID: actualCustomer,
                        customername: row[customer_name].ToString(),
                        shipInstruction: actualShipInstr,
                        machinedItem: row[machined_item_code].ToString(),
                        forgedItem: row[forged_item_code].ToString(),
                        gacType: actualGacCode,
                        planProdDate: actualPlanProdDate,
                        box: row[box].ToString(),
                        actuallocation:actualLocation,
                        originalTimestamp:row.Field<DateTime>(timestamp)
                        );


                    if (items.Select(selectString).Length == 0)
                    {
                        numOfNotFoundItemInSO++;
                        if (!listOfNotFoundItems.Contains(row[item_code].ToString()))
                        {
                            listOfNotFoundItems.Add(row[item_code].ToString());
                        }
                        actualOrder.AddError(missingItemString);
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    Console.WriteLine(row[order_code].ToString() + ':' + actualPlanProdDate);
                }
            }
            if (numOfNotFoundItemInSO > 0)
            {
                Console.WriteLine("\nNem található tétel {0} vevői rendelés esetén", numOfNotFoundItemInSO);
                foreach (var item in listOfNotFoundItems)
                {
                    Console.WriteLine(item);
                }
            }

            if (EnteredSalesOrders.Count > 0)
            {
                Console.WriteLine("\nA következő Sales orderek Entered státusszal rendelkeznek: ");
                foreach (var item in EnteredSalesOrders)
                {
                    Console.WriteLine(item);
                }
            }
        }
        /// <summary>
        /// Generates the Inventory orders from erp database
        /// </summary>
        private void GenerateInventoryOrders()
        {
            Console.WriteLine(updateInventoryOrder);
            DataTable source = asprova.sources[inventoryString];
            DataTable items = asprova.sources[itemString];

            foreach (DataRow row in source.Rows)
            {
                string myInvOrderStat = string.Empty;
                string myInvOrderQAStat = string.Empty;
                string myWipId = string.Empty;
                string foundWipSalesOrder = null;

                try
                {
                    string itemCode = row[item_code].ToString();
                    if (itemCode.ToUpper().Contains(gax) || itemCode.ToUpper().Contains(gaz)) // Ha gax vagy gaz, akkor ne vegye át
                    {
                        continue;
                    }

                    if (itemCode != null && itemCode != "")
                    {
                        if (itemCode[0] != '5' && itemCode[0] != 'G')
                        {
                            itemCode = "GA" + itemCode;
                        }
                    }
                    else
                    {
                        Console.WriteLine("Inventory item error: palette({0})", row[inventory_palette].ToString());
                    }

                        myInvOrderStat = row[inventory_status].ToString();
                        myInvOrderQAStat = row[inventory_qa_status].ToString();

                        if (!GetInventoryStatus(ref myInvOrderStat, ref myInvOrderQAStat))
                        {
                            Console.WriteLine("Error in inventory status: Item: {0}; Status: {1}; QA Status: {2}", itemCode, row[inventory_status].ToString(), row[inventory_qa_status].ToString());
                        }

                    //find SalesOrder to inventory

                    myWipId = row[wipId].ToString();

                    if (myWipId!=null&&myWipId!="")
                    {
                        foundWipSalesOrder = findInventorySalesOrder(myWipId);
                    }

                    

                        var actualOrder = GenerateOrderObject(
                            code: "I-" + itemCode + "_" + row[inventory_status].ToString() + "_" + row[inventory_qa_status].ToString() + "_" + row[inventory_palette].ToString() + "_" + row[inventory_type].ToString(),
                            itemCode: itemCode,
                            erpType: OrderType.InventoryRel,
                            let: DateTime.Today.AddYears(-5),
                            qtyOpen: Convert.ToInt32(row[inventory_qty]),
                            inventoryStatus: myInvOrderStat,
                            inventoryQAStatus: myInvOrderQAStat,
                            inventoryPalette: row[inventory_palette].ToString(),
                            inventoryType : Convert.ToString(row[inventory_type]).ToString(),
                            actualWipSalesOrder: foundWipSalesOrder,
                            originalTimestamp: row.Field<DateTime>(timestamp)
                            );

                        actualOrder.GacCode = row[gac_codeString].ToString();
                    if (!actualOrder.GacCode.Contains("GAC")&& actualOrder.GacCode!=null&&actualOrder.GacCode!="")
                    {
                        actualOrder.GacCode = "GAC" + actualOrder.GacCode;
                    }

                    // Szerepel-e az item táblában a tétel
                    string selectString = String.Format("{0}='{1}'", item_code, itemCode);
                    if (items.Select(selectString).Length == 0)
                    {
                        actualOrder.AddError(missingInventoryItem);

                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());

                }
            }
        }

        public Order GenerateOrderObject(string code, string itemCode, OrderType erpType, string erpCode = null, int qtyOrd = 0, int qtyOpen = 0, DateTime? let = null,
            string customerID = null, string customername = null, string shipInstruction = null, string machinedItem = null, string forgedItem = null, string inventoryStatus = null,
            string inventoryQAStatus = null, string box = null, string actuallocation = null, string inventoryPalette = null, string inventoryType = null, string gacType = null,
            string actualWipSalesOrder = null, DateTime? planProdDate = null, DateTime? originalTimestamp = null) //string planProdDateString = null)
        {
            // Conversion for Asprova
            let = let.Value.AddHours(6);

            // Create object
            Order order = new Order()
            {
                Code = code,
                ERPCode = erpCode,
                Item = itemCode,
                DueDate = let,
                CustomerID = customerID,
                CustomerName = customername,
                ShippingInstr = shipInstruction,
                QuantityOpen = qtyOpen,
                QuantityOrdered = qtyOrd,
                Type = erpType,
                MachinedItem = machinedItem,
                ForgedItem = forgedItem,
                InventoryStaus = inventoryStatus,
                InventoryQAStatus = inventoryQAStatus,
                InventoryPalette = inventoryPalette,
                InventoryType = inventoryType,
                GacCode = gacType,
                Box = box,
                Location = actuallocation,
                WipSalesOrder = actualWipSalesOrder,
                OriginalTimestamp=originalTimestamp
            };

            if (planProdDate != DateTime.MinValue)
            {
                order.PlanProdDate = planProdDate;
            }

            AddChild(order);

            return order;
        }
        /// <summary>
        /// Find GAC code to actual Sales Order
        /// </summary>
        /// <param name="itemCode"></param>
        /// <param name="customerCode"></param>
        /// <param name="location"></param>
        /// <param name="packInstr"></param>
        /// <returns></returns>
        public string FindGacCode(string itemCode, string customerCode, string location, string packInstr)
        {
            string foundGac = "noGAC";
            DataTable gacSource = asprova.sources[gacCodeString];

            foreach (DataRow dr in gacSource.Rows)
            {
                // Match: item, customer, location, pack type
                if (dr[item_code].ToString() == itemCode && dr[customer_code].ToString() == customerCode
                    && dr[locationString].ToString().ToUpper() == location.ToUpper() /*&& dr[pack_type].ToString() == packInstr*/)
                {
                    foundGac = dr[gacCodeString].ToString();
                   
                    break;
                }
            }
            return foundGac;
        }

        /// <summary>
        /// Inventory type and QA status
        /// </summary>
        /// <param name="statusCode"></param>
        /// <param name="statusQaCode"></param>
        /// <returns></returns>
        public bool GetInventoryStatus(ref string statusCode,ref string statusQaCode)
        {
            DataTable statusSource = asprova.sources[inventoryStatus];
            DataTable qaStatusSource = asprova.sources[inventoryQAStatus];

            bool foundStat = false;
            bool foundQaStat = false;

            // Find status
            foreach (DataRow dr in statusSource.Rows)
            {
                if (dr[inventoryStatusCode].ToString() == statusCode)
                {
                    statusCode = dr[inventoryStatusDesc].ToString();
                    foundStat = true;
                    break;
                }
            }

            // Find QA status
            foreach (DataRow dr in qaStatusSource.Rows)
            {
                if (dr[inventoryQAStatusCode].ToString() == statusQaCode)
                {
                    statusQaCode = dr[inventoryQAStatusDesc].ToString();
                    foundQaStat = true;
                    break;
                }
            }

            if (!foundStat || !foundQaStat)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Find Sales Order to actual Inventory
        /// </summary>
        /// <param name="wipId"></param>
        /// <returns></returns>
        public string findInventorySalesOrder(string wipId)
        {
            string foundSalesOrder = null;
            DataTable wipSales = asprova.sources[WipToSalesString];

            foreach (DataRow dr in wipSales.Rows)
            {
                if (dr[wip].ToString()==wipId)
                {
                    foundSalesOrder = dr[order_code].ToString();
                    break;
                }                
            }
            return foundSalesOrder;

        }
    }
}
