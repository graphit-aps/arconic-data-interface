﻿using graphIT.Asprova;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using static graphIT.Asprova.AsConst.Item;
using static graphIT.Arconic.AsprovaParser.ItemTable;

namespace graphIT.Arconic.AsprovaParser
{
    [DebuggerDisplay("{Code}")]
    public class Item : AsObject
    {
        #region "Database fields"
        [Db("item_code")]
        public string Code { get; set; }

        [Db("item_type")] // Asprova type: 0,M,Raw material,,;1,U,Purchased part,,;2,H,Manufactured part,,;3,I,Intermediate item,,;4,P,Finished item,,
        public ItemType ItemType { get; set; }

        [Db("item_volume")] // 
        public string ItemVolume { get; set; }

        [Db("erp_type")]
        public string ERPType { get; set; }

        [Db("replenish_flag")]
        public Replenishment ReplenishFlag { get; set; }

        [Db("obtain_method")]
        public ObtainMethod ObtainMethod { get; set; }

        [Db("item_prio")]
        public int? ItemPrio { get; set; }

        [Db("forged_item")] // 
        public string ForgedItem { get; set; }

        [Db("pegging_condition")] // 
        public string PeggingCond { get; set; }

        [Db("palette_input")] // 
        public string PaletteInput { get; set; }

        [Db("lot_size_min")]
        public int? LotSizeMin { get; set; }

        [Db("lot_size_unit")]
        public int? LotSizeUnit { get; set; }

        [Db("lot_sizing_period")]
        public string LotSizingPeriod { get; set; }

        [Db("lot_sizing_cycle")]
        public string LotSizingCycle { get; set; }

        [Db("lot_sizing_start")]
        public string LotSizingStart { get; set; }
                
        [Db("system_llc")] // 
        public int SystemLLC { get; set; }

        [Db("user_llc")] // 
        public int UserLLC { get; set; }

        [Db("pack_ship_lead_time")]
        public string PackShipLeadTime { get; set; }

        [Db("lot_size_round_qty")] // 
        public int LotSizeRoundQty { get; set; }

        [Db("peg_to_future")] // 
        public int PegToFuture { get; set; }

        [Db("erp_property")] // 
        public string ERPProperty { get; set; }

        [Db("erp_nomin")] // 
        public string ERPNomin { get; set; }

        [Db("has_yellow")] // 
        public int HasYellowResource { get; set; }

        [Db("mcb_item")] // 
        public string McbItem { get; set; }

        #endregion

        #region "Declaration"
        /// <summary>
        /// Item is generated only for Asprova
        /// </summary>
        public bool IsAsprovaObject { get; set; } = false;

        public bool IsSales { get; set; } = false;

        /// <summary>
        /// Asprova related link count
        /// </summary>
        public int referenced = 0;
        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="code">Unique ID for Item</param>
        /// <param name="name">Name</param>
        public Item(string code)
        {
            Code = code;
        }
    }
}
