﻿
namespace graphIT.Asprova
{
    public static partial class AsConst
    {
        public static partial class Item
        {
            public partial class ItemType : MyEnum<string>
            {
                public static ItemType C_Sales { get { return new ItemType("S", "C_Sales"); } }
            }
        }

        public static partial class Order
        {
            public partial class OrderType : MyEnum<string>
            {
            }
        }
    }
}
