﻿using graphIT.Asprova;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using static graphIT.Asprova.AsConst.Order;

namespace graphIT.Arconic.AsprovaParser
{
    [DebuggerDisplay("{Code}")]
    public class Order : AsObject
    {
        #region "Database fields"
        [Db("order_code")]
        public string Code { get; set; }

        [Db("order_erp_code")]
        public string ERPCode { get; set; }

        [Db("order_type")]
        public OrderType Type { get; set; }

        [Db("ordered_item")]
        public string Item { get; set; }

        [Db("due_date")]
        public DateTime? DueDate { get; set; }

        [Db("order_qty")]
        public double QuantityOrdered { get; set; }

        [Db("open_qty")]
        public double QuantityOpen { get; set; }

        [Db("order_priority")]
        public double OrderPriority { get; set; } = 80;

        [Db("customer_code")]
        public string CustomerID { get; set; }

        [Db("customer_name")]
        public string CustomerName { get; set; }

        [Db("shipping_instr")]
        public string ShippingInstr { get; set; }

        [Db("order_priority")]
        public double? Priority { get; set; }

        [Db("forged_die_num")]
        public string ForgedItem { get; set; }

        [Db("machined_die_num")]
        public string MachinedItem { get; set; }

        [Db("disabled")]
        public bool? Disabled { get; set; }

        [Db("comment")]
        public string Comment { get; set; }

        [Db("inventory_status")]
        public string InventoryStaus { get; set; }

        [Db("inventory_qa_status")]
        public string InventoryQAStatus { get; set; }

        [Db("inventory_palette")]
        public string InventoryPalette { get; set; }

        [Db("inventory_type")]
        public string InventoryType { get; set; }

        [Db("gac_code")]
        public string GacCode { get; set; }   //TODO: még hiányzik a táblából a gac kód

        [Db("plan_prod_date")]
        public DateTime? PlanProdDate { get; set; }

        [Db("box")]
        public string Box { get; set; }

        [Db("location")]
        public string Location { get; set; }

        [Db("wip_sales_order_code")]
        public string WipSalesOrder { get; set; }

        [Db("OriginalTimestamp")]
        public DateTime? OriginalTimestamp { get; set; }

        #endregion
    }
}
