﻿using graphIT.Asprova;
using System.Collections.Generic;
using System.Diagnostics;
using static graphIT.Arconic.AsprovaParser.Master;
using static graphIT.Arconic.AsprovaParser.Master.ProcessSelector;
using static graphIT.Arconic.AsprovaParser.Master.ProcessSelector.Process;
using static graphIT.Arconic.AsprovaParser.Master.ProcessSelector.Process.TaskSelector;
using static graphIT.Arconic.AsprovaParser.ItemTable;
using static graphIT.Asprova.AsConst.Master;
using static graphIT.Asprova.AsConst.Item;
using System;
using System.Data;
using System.Linq;

namespace graphIT.Arconic.AsprovaParser
{
    public class MasterTable : AsTable<Master>
    {
        private Dictionary<int, List<string>> operators = new Dictionary<int, List<string>>();

        public MasterTable(AsBase asprova) :  base(asprova)
        {
            databaseTableName = "asp_master";

            asprova.AddSource("masters", @"
                        SELECT erp_master.final_item_code 'final_item_code', 
                               erp_master.darabjegyzek 'darabjegyzek',
                               erp_master.anyagfajta 'final_type_erp',
                               erp_master.bazismenny 'bazis',
                               erp_master.komponens 'komponens',
                               erp_master.komp_menny 'kompmenny'
                        FROM erp_master
                        WHERE erp_master.komp_egys like 'KG'");

            asprova.AddSource("usr_katalog", @"
                        SELECT  vevo_code 'customer',
                                folia_type_katalog 'catalogtype',
                                porkeverek_code 'porkeverek'
                        FROM usr_katalogus");

            DownloadTargetDataSchema();
        }

        /// <summary>
        /// Generate Masters
        /// </summary>
        public override void Generate()
        {
            if (Generated) return;
            Generated = true;
        }
    }
}
