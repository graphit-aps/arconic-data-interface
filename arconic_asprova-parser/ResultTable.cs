﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using graphIT.Asprova;
using System.Data;

namespace graphIT.Arconic.AsprovaParser
{
    public class ResultTable : AsTable<Result>
    {
        private bool legacy = false;

        #region Used Strings
        private readonly string wip_code = "wip_code";
        private readonly string wip_item = "wip_item";
        private readonly string wip_plan_date = "wip_plan_date";
        private readonly string wip_start_day = "wip_start_day";
        private readonly string finish_date = "finish_date";
        private readonly string finish_time = "finish_time";
        private readonly string start_date = "start_date";
        private readonly string start_time = "start_time";
        private readonly string pack_code = "pack_code";
        private readonly string wip_machine = "wip_machine";
        private readonly string open_qty = "open_qty";
        private readonly string comp_qty = "comp_qty";
        private readonly string ordered_qty = "ordered_qty";
        private readonly string wip_status = "wip_status";
        private readonly string kataLine = "Kata-Line";
        private readonly string cdRdb = "CD RDB";
        private readonly string abRdb = "AB RDB";
        private readonly string abPolishing = "AB polishing";
        private readonly string cdPolishing = "CD polishing";
        private readonly string dB1 = "DB1";
        private readonly string dB2 = "DB2";
        private readonly string k2pol = "K2 polishing";
        private readonly string CD1 = "CD1";
        private readonly string CD2 = "CD2";
        private readonly string IMT = "IMT";
        private readonly string KIS = "KIS";
        private readonly string MEGA = "MEGA";
        private readonly string NAGY = "NAGY";
        private readonly string SS1 = "SS1";
        private readonly string SS2 = "SS2";
        private readonly string ULW = "ULW";
        private readonly string Forging = "Forging";
        readonly string wipSalesOrder = "wip_sales_order_code";
        readonly string wipSalesString = "wipSales";
        readonly string wip = "WIP";
        readonly string order_code = "order_code";
        #endregion

        public ResultTable(AsBase asprova, bool legacy = false) : base(asprova)
        {
            this.legacy = legacy;

            databaseTableName = "erp_wip";
            NoDeleteFlag = true;

            if (legacy)
            {
                asprova.AddSource("wips", @"
                          SELECT WIP_ID 'wip_code', 
                              ITEM_ID 'wip_item',
                              WIP_DATE 'wip_plan_date',
                              FIN_DATE 'finish_date',
                              FIN_TIME 'finish_time',
                              ST_DATE 'start_date',
                              ST_TIME 'start_time',
                              TVCSOM 'pack_code',
                              AWPE_PLANT 'wip_machine',
                              QTY_REMA 'open_qty',
                              QTY_COMP 'comp_qty',
                              QTY_PLAN 'ordered_qty',
                              STATUS 'wip_status'
                          FROM awpe_wips
                          ");
            }
            else
            {
                asprova.AddSource("wips", @"SELECT  [LekerdezesDat] 'wip_plan_date',
[TervDátum] 'finish_date'
      ,[WIPSzám] 'wip_code'
      ,[ScheduledStartTime] 'finish_time'
      ,[Cikkek] 'wip_item'
      ,[CsomagolásiMód] 'pack_code'
      ,[KezdoDb] 'ordered_qty'
      ,[KészDb] 'comp_qty' 
      ,[JobStatus] 'wip_status'
      ,[awpePlant] 'wip_machine'
  FROM [ASPROVA].[dbo].[awpe_ora_daily_wip_status] where LekerdezesDat=(select max(awpe_ora_daily_wip_status.LekerdezesDat) from awpe_ora_daily_wip_status) and (Cikkek not like '%DFP')");
            }

            asprova.AddSource("ekanban", @"SELECT  [ID]
      ,[PLAN_ID] 'wip_code'
      ,convert(varchar(10),[RECDATE],126) 'wip_plan_date'
      ,[PLANTYPE] 'wip_status'
      ,[STATION_DESC] 'wip_machine'
      ,[PLAN_START] 'finish_date'
      ,[WHEELTYPE] 'wip_item'
      ,[QUANTITY_PLAN] 'ordered_qty'
      ,[QUANTITY_READY] 'comp_qty' 
      ,[QUANTITY_REMAINING]
  FROM [ASPROVA].[dbo].[awpe_mach_ekanban] where RECDATE=(SELECT TOP(1)
      [RECDATE]
      
  FROM [ASPROVA].[dbo].[awpe_mach_ekanban] 
  group by RECDATE 
  order by ABS(DATEDIFF(second, convert(varchar(10),(SELECT TOP (1) [LekerdezesDat]
      
  FROM [ASPROVA].[dbo].[awpe_ora_daily_wip_status] order by LekerdezesDat desc),126) + ' 06:00:00', Recdate)) )
  union SELECT  [ID]
      ,[PLAN_ID] 'wip_code'
      ,convert(varchar(10),[RECDATE],126) 'wip_plan_date'
      ,[PLANTYPE] 'wip_status'
      ,[STATION_DESC] 'wip_machine'
      ,[PLAN_START] 'finish_date'
      ,[WHEELTYPE] 'wip_item'
      ,[QUANTITY_PLAN] 'ordered_qty'
      ,[QUANTITY_READY] 'comp_qty' 
	  ,[QUANTITY_PLAN]-[QUANTITY_READY] as [QUANTITY_REMAINING]
      
  FROM [ASPROVA].[dbo].[awpe_forg_ekanban] where RECDATE=(SELECT TOP(1)
      [RECDATE]
      
  FROM [ASPROVA].[dbo].[awpe_forg_ekanban] 
  group by RECDATE 
  order by ABS(DATEDIFF(second, convert(varchar(10),(SELECT TOP (1) [LekerdezesDat]
      
  FROM [ASPROVA].[dbo].[awpe_ora_daily_wip_status] order by LekerdezesDat desc),126) + ' 06:00:00', Recdate)) )
");

            asprova.AddSource(wipSalesString, @"
                     SELECT CONCAT(CAST(CAST(ORDER_NUMBER as int) as varchar), '_', LINE_NUMBER) 'order_code',
                         ORDER_NUMBER 'order_number',
                         LINE_NUMBER 'line_number',
                         WIP 'WIP',
                        START_QUANTITY 'start_qty',
                        QUANTITY_COMPLETED 'qty_completed',
                        STATUS 'status'
                        FROM[ASPROVA].[dbo].[awpe_ora_prod_demand_wips]");

            DownloadTargetDataSchema();
        }

        public override void Generate()
        {
            if (Generated) return;
            Generated = true;

            if (legacy)
            {
                GenerateResults_old();
            }
            else
            {
                GenerateResults();
                GenerateEkanbanResults();
            }
        }



        private void GenerateResults()
        {
            Console.WriteLine("Updating Work Order results");
            DataTable source = asprova.sources["wips"];
            DateTime currentTime = DateTime.Now;

            foreach (DataRow row in source.Rows)
            {
                string myWipCode = row[wip_code].ToString();
                string foundSalesOrder = null;

                Result actualwip = null;

                actualwip = GenerateResultObject(
                 wipCode: row[wip_code].ToString(),
                 wipItemCode: row[wip_item].ToString(),
                 wipPlanDate: row[wip_plan_date].ToString(),
                 wipFinishDate: row[finish_date].ToString(),
                 wipFinishTime: Convert.ToDateTime(row[finish_time].ToString()),

                 packCode: row[pack_code].ToString(),
                 compQty: Convert.ToInt32(row[comp_qty]),

                 wipMachine: row[wip_machine].ToString(),
                 wipStatus: row[wip_status].ToString(),
                 ordQty: Convert.ToInt32(row[ordered_qty])
                 );


                //find salesOrder to current WIP
                foundSalesOrder = findSalesToWip(myWipCode);
                if (foundSalesOrder != null && foundSalesOrder != "")
                {
                    actualwip.WipSalesOrder = foundSalesOrder;
                }
            }
        }


        private void GenerateEkanbanResults()
        {
            Console.WriteLine("Updating Ekanban results");
            DataTable source = asprova.sources["ekanban"];
            DateTime currentTime = DateTime.Now;

            foreach (DataRow row in source.Rows)
            {
                string myWipCode = row[wip_code].ToString();
                string foundSalesOrder = null;

                Result actualwip = null;

                actualwip = GenerateResultObject(
                 wipCode: row[wip_code].ToString(),
                 wipItemCode: row[wip_item].ToString(),
                 wipPlanDate: row[wip_plan_date].ToString(),
                 wipFinishDate: row[finish_date].ToString(),
                 wipFinishTime: Convert.ToDateTime("00:00:00"),

                 packCode: row[wip_status].ToString(),
                 compQty: Convert.ToInt32(row[comp_qty]),

                 wipMachine: row[wip_machine].ToString(),
                 wipStatus: "3",
                 ordQty: Convert.ToInt32(row[ordered_qty])
                 );


                //find salesOrder to current WIP
                //foundSalesOrder = findSalesToWip(myWipCode);
                //if (foundSalesOrder != null && foundSalesOrder != "")
                //{
                //    actualwip.WipSalesOrder = foundSalesOrder;
                //} 
            }
        }





        public Result GenerateResultObject(string wipCode, string wipItemCode, string wipPlanDate, string wipFinishDate, DateTime wipFinishTime,
            string packCode, int compQty, string wipMachine, string wipStatus, int ordQty)
        {
            DateTime actualPlanDate = new DateTime();
            DateTime actualFinishDate = new DateTime();
            DateTime actualStartDate = new DateTime();

            int openQty = 0;

            if (!wipItemCode.Contains("GA")&&wipItemCode!="TPM"&&wipMachine.ToUpper()!= "K1 FORGING")
            {
                wipItemCode = "GA" + wipItemCode;
            }

            string packTypeName = packCode;

            if (packCode.Length >= 2&&packCode!="ACTPLAN"&&packCode!="BACKLOG")
            {
                packCode = "GAC" + packCode.Substring(0, 2);
            }
            else
            {
                packCode = "noGAC";
            }


            //Machine conversion: AB RDB, CD RDB, Kata-Line, AB polishing, CD polishing, DB1, DB2
            if (wipMachine.IndexOf("K2_L1_POL") > -1) // K2 WIP irrelevant
            {
                wipMachine = k2pol;
            }
            else if (wipMachine.ToUpper().IndexOf("KAT") > 0)
            {
                wipMachine = kataLine;
            }
            else if (wipMachine.ToUpper().IndexOf("RDB") > 0)
            {
                if (wipMachine.ToUpper().IndexOf("CD") > 0)
                {
                    wipMachine = cdRdb;
                }
                else
                {
                    wipMachine = abRdb;
                }
            }
            else if (wipMachine.ToUpper().IndexOf("CD") > 0)
            {
                wipMachine = cdPolishing;
            }
            else if (wipMachine.ToUpper().IndexOf("AB") > 0)
            {
                wipMachine = abPolishing;
            }
            else if (wipMachine.ToUpper().IndexOf("DB1") > 0)
            {
                wipMachine = dB1;
            }
            else if (wipMachine.ToUpper().IndexOf("DB2") > 0)
            {
                wipMachine = dB2;
            }
            else if (wipMachine.ToUpper().IndexOf("CD SOR 1") > -1)
            {
                wipMachine = CD1;
            }
            else if (wipMachine.ToUpper().IndexOf("CD SOR 2") > -1)
            {
                wipMachine = CD2;
            }
            else if (wipMachine.ToUpper().IndexOf("IMT SOR") > -1)
            {
                wipMachine = IMT;
            }
            else if (wipMachine.ToUpper().IndexOf("KIS SOR") > -1)
            {
                wipMachine = KIS;
            }
            else if (wipMachine.ToUpper().IndexOf("MEGALINE") > -1)
            {
                wipMachine = MEGA;
            }
            else if (wipMachine.ToUpper().IndexOf("NAGY SOR") > -1)
            {
                wipMachine = NAGY;
            }
            else if (wipMachine.ToUpper().IndexOf("SUPER SINGLE 1") > -1)
            {
                wipMachine = SS1;
            }
            else if (wipMachine.ToUpper().IndexOf("SUPER SINGLE 2") > -1)
            {
                wipMachine = SS2;
            }
            else if (wipMachine.ToUpper().IndexOf("ULW SOR") > -1)
            {
                wipMachine = ULW;
            }
            else if (wipMachine.ToUpper().IndexOf("K1 FORGING") > -1)
            {
                wipMachine = Forging;
            }

            try
            {
                //wipPlanDate = DateTime.Today.Year.ToString().Substring(0,2) + wipPlanDate.Substring(0, 2) + "/" + wipPlanDate.Substring(2, 2) + "/" + wipPlanDate.Substring(4, 2);
                //actualPlanDate = Convert.ToDateTime(wipPlanDate).AddHours(6);

                //wipFinishDate = DateTime.Today.Year.ToString().Substring(0, 2) + wipFinishDate.Substring(0,2) + "/" + wipFinishDate.Substring(2, 2) + "/" + wipFinishDate.Substring(4, 2);
                wipFinishDate = wipFinishDate.Replace('-', '/');

                actualFinishDate = Convert.ToDateTime(wipFinishDate).AddHours(wipFinishTime.Hour);
                actualFinishDate = actualFinishDate.AddMinutes(wipFinishTime.Minute);
                actualFinishDate = actualFinishDate.AddSeconds(wipFinishTime.Second);

                //wipStartDate = DateTime.Today.Year.ToString().Substring(0, 2) +  wipStartDate.Substring(0, 2) + "/" + wipStartDate.Substring(2, 2) + "/" + wipStartDate.Substring(4, 2);
                //actualStartDate = Convert.ToDateTime(wipStartDate).AddHours(wipStartTime.Hour).AddMinutes(wipStartTime.Minute).AddSeconds(wipStartTime.Second);



                openQty = ordQty - compQty;

                if (wipStatus == "Complete")
                {
                    wipStatus = "4";
                }
                else if (wipStatus == "Released")
                {
                    wipStatus = "3";
                }
                else
                {
                    wipStatus = wipStatus.Substring(0,1);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Cannot convert time of WIP: {0}", wipCode);
                Console.WriteLine(ex.ToString());
            }

            Result result = new Result(wipCode, wipItemCode, compQty, openQty, packCode, wipStatus, wipPlanDate.ToString(), 
                actualFinishDate.ToString(), wipMachine, ordQty, System.Data.SqlTypes.SqlDateTime.MinValue.ToString(), System.Data.SqlTypes.SqlDateTime.MinValue.ToString(), packTypeName);

            AddChild(result);

            return result;
        }


        public string findSalesToWip(string WipCode)
        {

            DataTable wipSales = asprova.sources[wipSalesString];
            string foundSalesCode = null;

            foreach (DataRow dataRow in wipSales.Rows)
            {
                if (dataRow[wip].ToString()==WipCode)
                {
                    foundSalesCode = dataRow[order_code].ToString();
                    break;
                }
            }
            return foundSalesCode;


        }

        #region legacy
        private void GenerateResults_old()
        {
            Console.WriteLine("Updating Work Order results");
            DataTable source = asprova.sources["wips"];
            DateTime currentTime = DateTime.Now;

            foreach (DataRow row in source.Rows)
            {
                string myWipCode = row[wip_code].ToString();
                string foundSalesOrder = null;

                var actualwip = GenerateResultObject_old(
                     wipCode: row[wip_code].ToString(),
                     wipItemCode: row[wip_item].ToString(),
                     wipPlanDate: row[wip_plan_date].ToString(),
                     wipFinishDate: row[finish_date].ToString(),
                     wipFinishTime: Convert.ToDateTime(row[finish_time]),
                     wipStartDate: row[start_date].ToString(),
                     wipStartTime: Convert.ToDateTime(row[start_time].ToString()),
                     packCode: row[pack_code].ToString(),
                     compQty: Convert.ToInt32(row[comp_qty]),
                     openQty: Convert.ToInt32(row[open_qty].ToString()),
                     wipMachine: row[wip_machine].ToString(),
                     wipStatus: row[wip_status].ToString(),
                     ordQty: Convert.ToInt32(row[ordered_qty])
                     );

                //find salesOrder to current WIP
                foundSalesOrder = findSalesToWip(myWipCode);
                if (foundSalesOrder != null && foundSalesOrder != "")
                {
                    actualwip.WipSalesOrder = foundSalesOrder;
                }
            }
        }

        public Result GenerateResultObject_old(string wipCode, string wipItemCode, string wipPlanDate, string wipFinishDate, DateTime wipFinishTime,
            string wipStartDate, DateTime wipStartTime, string packCode, int compQty, int openQty, string wipMachine, string wipStatus, int ordQty)
        {
            DateTime actualPlanDate = new DateTime();
            DateTime actualFinishDate = new DateTime();
            DateTime actualStartDate = new DateTime();

            if (!wipItemCode.Contains("GA"))
            {
                wipItemCode = "GA" + wipItemCode;
            }




            packCode = "GAC" + packCode;

            //Machine conversion: AB RDB, CD RDB, Kata-Line, AB polishing, CD polishing, DB1, DB2
            if (wipMachine.IndexOf("K2") > 0) // K2 WIP irrelevant
            { }
            else if (wipMachine.ToUpper().IndexOf("KAT") > 0)
            {
                wipMachine = kataLine;
            }
            else if (wipMachine.ToUpper().IndexOf("RDB") > 0)
            {
                if (wipMachine.ToUpper().IndexOf("CD") > 0)
                {
                    wipMachine = cdRdb;
                }
                else
                {
                    wipMachine = abRdb;
                }
            }
            else if (wipMachine.ToUpper().IndexOf("CD") > 0)
            {
                wipMachine = cdPolishing;
            }
            else if (wipMachine.ToUpper().IndexOf("AB") > 0)
            {
                wipMachine = abPolishing;
            }
            else if (wipMachine.ToUpper().IndexOf("DB1") > 0)
            {
                wipMachine = dB1;
            }
            else if (wipMachine.ToUpper().IndexOf("DB2") > 0)
            {
                wipMachine = dB2;
            }

            try
            {
                wipPlanDate = DateTime.Today.Year.ToString().Substring(0, 2) + wipPlanDate.Substring(0, 2) + "/" + wipPlanDate.Substring(2, 2) + "/" + wipPlanDate.Substring(4, 2);
                actualPlanDate = Convert.ToDateTime(wipPlanDate).AddHours(6);
                wipFinishDate = DateTime.Today.Year.ToString().Substring(0, 2) + wipFinishDate.Substring(0, 2) + "/" + wipFinishDate.Substring(2, 2) + "/" + wipFinishDate.Substring(4, 2);
                actualFinishDate = Convert.ToDateTime(wipFinishDate).AddHours(wipFinishTime.Hour);
                actualFinishDate = actualFinishDate.AddMinutes(wipFinishTime.Minute);
                actualFinishDate = actualFinishDate.AddSeconds(wipFinishTime.Second);

                wipStartDate = DateTime.Today.Year.ToString().Substring(0, 2) + wipStartDate.Substring(0, 2) + "/" + wipStartDate.Substring(2, 2) + "/" + wipStartDate.Substring(4, 2);
                actualStartDate = Convert.ToDateTime(wipStartDate).AddHours(wipStartTime.Hour).AddMinutes(wipStartTime.Minute).AddSeconds(wipStartTime.Second);



            }
            catch (Exception ex)
            {
                Console.WriteLine("Cannot convert time of WIP: {0}", wipCode);
                Console.WriteLine(ex.ToString());
            }

            Result result = new Result(wipCode, wipItemCode, compQty, openQty, packCode, wipStatus, actualPlanDate.ToString(),
                actualFinishDate.ToString(), wipMachine, ordQty, actualStartDate.ToString(), actualPlanDate.AddDays(-1).ToString(), null);

            AddChild(result);

            return result;
        }
        #endregion

    }
}
