﻿using graphIT.Asprova;
using System.Collections.Generic;
using System.Diagnostics;
using static graphIT.Asprova.AsConst.Master;

namespace graphIT.Arconic.AsprovaParser
{
    /// <summary>
    /// Master Final Item
    /// </summary>
    [DebuggerDisplay("Master: {FinalItemCode}")]
    public class Master : AsObject
    {
        #region "Database fields"
        [Db("final_item_code")]
        public string FinalItemCode { get; set; }
        #endregion

        #region helper
        public List<ProcessSelector> ProcessSelectors { get { return AsBase.GetChildren<ProcessSelector>(children); } }
        #endregion


        public Master(string finalItemCode)
        {
            FinalItemCode = finalItemCode;
        }


        /// <summary>
        /// Master Process Selector
        /// </summary>
        [DebuggerDisplay("ProcSelector: {Code}")]
        public class ProcessSelector : AsObject
        {
            #region "Database fields"
            [Db("proc_selector_code")]
            public string Code { get; set; }

            [Db("proc_selector_valid_condition_expr")]
            public string ValidContiditon { get; set; }
            #endregion

            #region helper
            public List<Process> Processes { get { return AsBase.GetChildren<Process>(children); } }
            #endregion


            public ProcessSelector(Master master, string code, bool add = true)
            {
                table = master.table;
                parent = master;
                Code = code;

                if (add)
                    master.children.Add(this);
            }

            /// <summary>
            /// Master Process
            /// </summary>
            [DebuggerDisplay("Process: {Number} {Code}")]
            public class Process : AsObject
            {
                #region "Database fields"
                [Db("proc_no")]
                public int Number { get; set; }

                [Db("proc_code")]
                public string Code { get; set; }
                #endregion

                #region helper
                public List<TaskSelector> TaskSelectors { get { return AsBase.GetChildren<TaskSelector>(children); } }

                public List<InputInstruction> InputInstructions { get { return AsBase.GetChildren<InputInstruction>(children); } }

                public List<UseInstruction> UseInstructions { get { return AsBase.GetChildren<UseInstruction>(children); } }

                public List<OutputInstruction> OutputInstructions { get { return AsBase.GetChildren<OutputInstruction>(children); } }
                #endregion


                public Process(ProcessSelector procSelector, int number, string code, bool add = true)
                {
                    table = procSelector.table;
                    parent = procSelector;
                    Number = number;
                    Code = code;

                    if (add) procSelector.children.Add(this);
                }

                /// <summary>
                /// Master Task Selector
                /// </summary>
                [DebuggerDisplay("TaskSelector: {Code}")]
                public class TaskSelector : AsObject
                {
                    #region "Database fields"
                    [Db("task_selector_code")]
                    public string Code { get; set; } = string.Empty;

                    [Db("task_selector_valid_condition_expr")]
                    public string ValidCondition { get; set; }
                    #endregion

                    #region "helper"
                    public List<UseInstruction> UseInstructions { get { return AsBase.GetChildren<UseInstruction>(children); } }
                    #endregion

                    public TaskSelector(Process process, string code, bool add = true)
                    {
                        table = process.table;
                        parent = process;
                        Code = code;

                        if (add)
                            process.children.Add(this);
                    }

                    /// <summary>
                    /// Master Use Instruction
                    /// </summary>
                    [DebuggerDisplay("UseInstTs: {Code} - {Resource}")]
                    public class UseInstruction : UseInstructionCore
                    {
                        public UseInstruction(TaskSelector taskSelector, string code, string resource, bool add = true) : base(code, resource)
                        {
                            table = taskSelector.table;
                            parent = taskSelector;

                            if (add)
                                taskSelector.children.Add(this);
                        }
                    }
                }

                /// <summary>
                /// Master Input Instruction
                /// </summary>
                [DebuggerDisplay("InputInst: {Code} - {Item}")]
                public class InputInstruction : AsObject
                {
                    #region "Database fields"
                    // No task selector for this item, but db has this primary key, so we need this
                    [Db("task_selector_code")]
                    public string TaskSelectorCode { get; set; } = string.Empty;

                    [Db("instruction_type")]
                    public string Type { get; set; } = "I";

                    [Db("instruction_code")]
                    public string Code { get; set; }

                    [Db("item_or_res_code")]
                    public string Item { get; set; }

                    [Db("prev_proc_no")]
                    //public int? PrevProcessNumber { get; set; } = -1; //:előző verzió 2019.11.06
                    public int PrevProcessNumber { get; set; }

                    [Db("time_constraint_method")]
                    public TimeConstraintMethod TimeConstraintMethod { get; set; }

                    [Db("time_constraint_min")]
                    public string TimeConstraintMIN { get; set; }


                    public double? setup;
                    [Db("setup")]
                    public string Setup
                    {
                        get
                        {
                            return setup == null ? null : ((double)setup).ToString("0.#########################").Replace(",", ".");
                        }
                    }



                    public double? production;
                    [Db("production")]
                    public string Production
                    {
                        get
                        {
                            return production == null ? null : ((double)production).ToString("0.#########################").Replace(",", ".");
                        }
                    }

                    [Db("instruction_valid_condition_expr")]
                    public string InstructionValidConditionExpr { get; set; }
                    #endregion


                    public InputInstruction(Process process, string code, string item, bool add = true)
                    {
                        table = process.table;
                        parent = process;
                        Code = code;
                        Item = item;

                        if (add) process.children.Add(this);
                    }
                }

                /// <summary>
                /// Master Use Instruction
                /// </summary>
                [DebuggerDisplay("UseInst: {Code} - {Resource}")]
                public class UseInstruction : UseInstructionCore
                {
                    #region "Database fields"
                    [Db("task_selector_code")]
                    public string TaskSelectorCode { get; set; } = string.Empty;
                    #endregion


                    public UseInstruction(Process process, string code, string resource, bool add = true) : base(code, resource)
                    {
                        table = process.table;
                        parent = process;


                        if (add) process.children.Add(this);
                    }
                }

                /// <summary>
                /// Master Output Instruction
                /// </summary>
                [DebuggerDisplay("OutputInst: {Code} - {ItemCode}")]
                public class OutputInstruction : AsObject
                {
                    #region "Database fields"
                    // No task selector for this item, but db has this primary key, so we need this
                    [Db("task_selector_code")]
                    public string TaskSelectorCode { get; set; } = string.Empty;

                    [Db("instruction_type")]
                    public string Type { get; set; } = "O";

                    [Db("instruction_code")]
                    public string Code { get; set; }

                    [Db("item_or_res_code")]
                    public string Item { get; set; }

                    [Db("setup")]
                    public string Setup { get; set; }

                    [Db("production")]
                    public string Production { get; set; }

                    [Db("teardown")]
                    public string Teardown { get; set; }

                    [Db("scrap")]
                    public double? Scrap { get; set; }

                    [Db("yield")]
                    public double? Yield { get; set; }
                    #endregion


                    public OutputInstruction(Process process, string code, string item, bool add = true)
                    {
                        table = process.table;
                        parent = process;
                        Code = code;
                        Item = item;

                        if (add) process.children.Add(this);
                    }
                }

                /// <summary>
                /// Master Use Instruction
                /// </summary>
                [DebuggerDisplay("UseInstTs: {Code} - {Resource}")]
                public class UseInstructionCore : AsObject
                {
                    #region "Database fields"
                    [Db("instruction_type")]
                    public string Type { get; set; } = "U";

                    [Db("instruction_code")]
                    public string Code { get; set; }

                    [Db("item_or_res_code")]
                    public string Resource { get; set; }

                    [Db("setup")]
                    public string Setup { get; set; }

                    [Db("production")]
                    public string Production { get; set; }

                    [Db("teardown")]
                    public string Teardown { get; set; }

                    [Db("resource_qty")]
                    public double? ResourceQty { get; set; }

                    [Db("instruction_valid_condition_expr")]
                    public string ValidConditionExpr { get; set; }

                    [Db("time_constraint_method")]
                    public TimeConstraintMethod TimeConstraintMethod { get; set; }

                    [Db("time_constraint_min")]
                    public string TimeConstraintMIN { get; set; }
                    #endregion

                    #region "custom setters"
                    public void SetSetup(double? time)
                    {
                        if (time == null || time == 0) return;
                        Setup = string.Format("{0}", time.ToString().Replace(",", "."));
                    }

                    public void SetSetup(string expression)
                    {
                        Setup = expression;
                    }

                    public void SetProduction(double? time)
                    {
                        if (time == null && time == 0) return;
                        Production = string.Format("1mp*{0}*(HOLDER.ResourceQty/IF(FValid(ME.RequiredResQty),ME.RequiredResQty,HOLDER.ResourceQty))", (((double)time)).ToString("0.#####").Replace(",", "."));
                    }
                    #endregion

                    public UseInstructionCore(string code, string resource)
                    {
                        Code = code;
                        Resource = resource;
                    }
                }
            }
        }
    }
}
