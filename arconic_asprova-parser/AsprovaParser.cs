﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace graphIT.Arconic.AsprovaParser
{
    public class AsprovaParser
    {
        public static JSON config;

        public static SqlTransaction transaction = null;
        public static SqlConnection conn = null;

        public static Asprova asprova;

        public AsprovaParser(JSON cfg)
        {
            // When using debugger, no error handling
            if (Debugger.IsAttached)
            {
                AsprovaParser2(cfg);
            }
            else
            {
                try
                {
                    AsprovaParser2(cfg);
                } catch
                {
                    // Error handling, rollback any changes then throw error to upper level
                    if (transaction != null) transaction.Rollback();
                    CloseConnection();
                    throw;
                }
            }
        }

        /// <summary>
        /// Real constructor
        /// </summary>
        /// <param name="cfg">JSON configuration file</param>
        private void AsprovaParser2(JSON cfg)
        {
            config = cfg;

            bool legacy = false;

            if (config.Legacy.ToLower() == "false")
            {
                legacy = false;
            }
            else if (config.Legacy.ToLower() == "true")
            {
                legacy = true;
            }
            else
            {
                legacy = false;
            }


            InitConnection(config.ConnectionStringMSSQL);

            asprova = new Asprova(ref conn);
            asprova.Run(legacy);

            transaction.Commit();
            CloseConnection();
        }

        /// <summary>
        /// Init MySQL data connection
        /// </summary>
        private void InitConnection(string connectionString)
        {
            conn = new SqlConnection(connectionString);
            conn.Open();
            transaction = conn.BeginTransaction();
        }

        /// <summary>
        /// Close MySQL data connection
        /// </summary>
        private void CloseConnection()
        {
            if (conn == null) return;

            if (conn.State != ConnectionState.Closed) conn.Close();
            conn.Dispose();
            conn = null;
        }
    }
}
