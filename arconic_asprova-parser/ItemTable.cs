﻿using graphIT.Asprova;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using static graphIT.Asprova.AsConst;
using static graphIT.Asprova.AsConst.Item;
using System.Globalization;

namespace graphIT.Arconic.AsprovaParser
{
    public class ItemTable : AsTable<Item>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="asprova">Parent Asprova object</param>
        public ItemTable(AsBase asprova) : base(asprova)
        {
            databaseTableName = "asp_item";

            DownloadTargetDataSchema();
        }

        public Item GenerateItemObject(DataRow row, ItemCType type, bool add = true, Item reference = null)
        {
            // Create object
            string myItemCode = "NoItemCode";
            Item item = new Item(myItemCode);

            return item;
        }

        private void SetItemParameters(Item item, ItemCType type, Item reference = null)
        {
  
        }

        /// <summary>
        /// Generate Items
        /// </summary>
        public override void Generate()
        {
            if (Generated) return;
            Generated = true;

            Console.WriteLine("Updating Item Master");

            foreach (DataRow row in asprova.sources["items"].Rows)
            {
                GenerateItemObject(row, ItemCType.Final);
            }
        }

        // Ez szerintem egy teljesen fölöslegesen bevezetett új típus az anyag típusra, de KEFÓn is használtam
        public class ItemCType : MyEnum<string>
        {
            private ItemCType(string value, string name) : base(value, name) { }

            public static ItemCType Final { get { return new ItemCType("Final", "Final"); } }
            public static ItemCType Raw { get { return new ItemCType("Raw", "Raw"); } }
            public static ItemCType Produced { get { return new ItemCType("Produced", "Produced"); } }
            public static ItemCType Sales { get { return new ItemCType("Sales", "Sales"); } }
            public static ItemCType Purchasemaster { get { return new ItemCType("Purchasemaster", "Purchasemaster"); } }
            public static ItemCType Salesmaster { get { return new ItemCType("Salesmaster", "Salesmaster"); } }
            public static ItemCType Intermediate { get { return new ItemCType("Intermediate", "Intermediate"); } }
        }

    }
}
