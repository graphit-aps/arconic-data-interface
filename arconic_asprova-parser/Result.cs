﻿using graphIT.Asprova;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace graphIT.Arconic.AsprovaParser
{
    public class Result : AsObject
    {
        #region "Database fields"
        [Db("wip_code")]
        public string WipCode { get; set; }

        [Db("wip_item_code")]
        public string WipItem { get; set; }

        [Db("wip_comp_qty")]
        public int WipCompQty { get; set; }

        [Db("wip_open_qty")]
        public int WipOpenQty { get; set; }

        [Db("wip_ordered_qty")]
        public int WipOrderedQty { get; set; }

        [Db("pack_type")]
        public string PackingType { get; set; }

        [Db("pack_type_name")]
        public string PackingTypeName { get; set; }

        [Db("wip_status")]
        public string WipStatus { get; set; }

        [Db("wip_plan_time")]
        public string WipPlanTime { get; set; }

        [Db("wip_finish_time")]
        public string WipFinishTime { get; set; }

        [Db("wip_start_time")]
        public string WipStartTime { get; set; }

        [Db("wip_start_day")]
        public string WipStartDay { get; set; }

        [Db("wip_machine")]
        public string WipMachine { get; set; }

        [Db("wip_sales_order_code")]
        public string WipSalesOrder { get; set; }

        #endregion

        public Result(string code, string item, int compQty, int openQty, string packType, string wipSatus, string wipPlanDate,
            string wipFinishDate, string wipMachine, int ordQty, string wipStartDate, string wipStartDay, string packTypeName)
        {
            WipCode = code;
            WipItem = item;
            WipCompQty = compQty;
            WipOpenQty = openQty;
            WipStatus = wipSatus;
            WipPlanTime = wipPlanDate;
            WipFinishTime = wipFinishDate;
            WipMachine = wipMachine;
            PackingType = packType;
            WipOrderedQty = ordQty;
            WipStartTime = wipStartDate;
            WipStartDay = wipStartDay;
            PackingTypeName = packTypeName;
        }
    }
}
