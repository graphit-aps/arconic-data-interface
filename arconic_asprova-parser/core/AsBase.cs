﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using Newtonsoft.Json;
using graphIT.Arconic.AsprovaParser;
using System.Text;

namespace graphIT.Asprova
{
    public abstract class AsBase
    {
        private List<Source> sourceDefinitions = new List<Source>();
        public Dictionary<string, DataTable> sources = new Dictionary<string, DataTable>();

        public Dictionary<string, AsTable> tables = new Dictionary<string, AsTable>();

        public SqlConnection conn;

        public AsBase(SqlConnection conn)
        {
            this.conn = conn;
        }


        /// <summary>
        /// Add new table for tables.
        /// </summary>
        /// <param name="table">The table what we would add to tables.</param>
        public void AddTable(AsTable table)
        {
            tables.Add(table.name, table);
        }

        /// <summary>
        /// Get a table from tables.
        /// </summary>
        /// <param name="table">Table name.</param>
        /// <returns></returns>
        public AsTable GetTable(string table)
        {
            return tables[table];
        }

        /// <summary>
        /// 
        /// </summary>
        public void Parse()
        {
            Console.WriteLine($"Downloading data");
            DownloadSources();
            Console.WriteLine($"Generating data");
            Generate();
            Console.WriteLine($"Uploading data");
            Upload();
        }

        /// <summary>
        /// Download the source data from the datatables what we given in the OrderTable and MasterTable classes.
        /// </summary>
        private void DownloadSources()
        {
            //Create new connection (opening a new MS SQL connection)
            string[] cfgLines;
            cfgLines = File.ReadAllLines(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "configs/asprova.json"), Encoding.UTF8);
            JSON cfgParser = JsonConvert.DeserializeObject<JSON>(string.Join("\r\n", cfgLines));
            SqlConnection actualConn = new SqlConnection(cfgParser.ConnectionStringMSSQL);
            try
            {
                using (actualConn)
                {
                    actualConn.Open();
                    foreach (Source s in sourceDefinitions)
                    {
                        sources.Add(s.name, new DataTable());
                        var adapter = new SqlDataAdapter(s.query, actualConn);
                        adapter.Fill(sources[s.name]);
                    }
                }
            }
            catch(SqlException ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                actualConn.Close();
            }
        }

        /// <summary>
        /// Generate the table objects for each tables.
        /// </summary>
        private void Generate()
        {
            foreach (AsTable t in tables.Values)
            {
                Console.WriteLine($" {t.name}");
                t.Generate();
            }
        }

        /// <summary>
        /// Download the current state of database. Synchronize the current database state with the new values and upload the new or modified values to database. 
        /// </summary>
        private void Upload()
        {
            foreach (AsTable t in tables.Values)
            {
                Console.WriteLine($" {t.name}");

                t.DownloadTargetData();

                t.Save();
            }
        }

        /// <summary>
        /// Add new source to sources
        /// </summary>
        /// <param name="name">The name of the source</param>
        /// <param name="query">MySQL query</param>
        public void AddSource(string name, string query)
        {
            sourceDefinitions.Add(new Source(name, query));
        }

        // Source class
        public class Source
        {
            public string name;
            public string query;

            public Source(string name, string query)
            {
                this.name = name;
                this.query = query;
            }
        }

        public static List<T> GetChildren<T>(List<AsObject> children) where T : AsObject
        {
            List<T> result = new List<T>();

            foreach (dynamic item in children)
            {
                if (item is T)
                    result.Add(item as T);
            }

            return result;
        }
    }
}
