﻿using System;

namespace graphIT.Asprova
{
    /// <summary>
    /// Attribute.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class DbAttribute : Attribute
    {
        /// <summary>
        /// String field.
        /// </summary>
        string _field;

        /// <summary>
        /// Attribute constructor.
        /// </summary>
        public DbAttribute()
        {
        }

        /// <summary>
        /// Attribute constructor.
        /// </summary>
        public DbAttribute(string field)
        {
            _field = field;
        }

        /// <summary>
        /// Get and set.
        /// </summary>
        public string Field
        {
            get { return _field; }
            set { _field = value; }
        }
    }
}
