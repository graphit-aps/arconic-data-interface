﻿//using MySql.Data.MySqlClient;
using System.IO;
using Newtonsoft.Json;
using graphIT.Arconic.AsprovaParser;
using System.Text;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Data.SqlClient;

namespace graphIT.Asprova
{
    // extend abstract Metadata class
    public abstract class AsTable<T> : AsTable where T : AsObject
    {
        public AsTable(AsBase asprova) : base(asprova) { }
    }

    public abstract class AsTable : Dictionary<string, AsObject>
    {
        public string name;

        public AsBase asprova;

        public string databaseTableName;

        public string[] cfgLines;
        public JSON cfgParser = new JSON();

        // Fields
        public string fieldTrackChanges = "isTemp";
        public string fieldStatusFlag = "status_flag";
        public string fieldCreatedAt = "created_at";
        public string fieldLastUpdate = "last_update";

        public bool NoDeleteFlag {get; set;}

        // Fields - revision
        public string fieldReference = null;
        public string fieldRevision = null;
        public string fieldValidFrom = null;
        public string fieldValidTo = null;

        // Database target
        public DataTable target = new DataTable();
        public SqlDataAdapter adapterTarget;

        // Database Primary keys
        public List<string> columns = new List<string>();
        public List<string> primaryKeys = new List<string>();
        public HashSet<string> firstPrimaryKeyValues = new HashSet<string>();
        

        public bool Generated { get; set; } = false;
        public abstract void Generate();
        public bool Saved { get; set; } = false;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="asprova">Parent Asprova object</param>
        /// <param name="name">Table unique name for C#</param>
        public AsTable(AsBase asprova)
        {
            this.asprova = asprova;

           cfgLines = File.ReadAllLines(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "configs/asprova.json"), Encoding.UTF8);
           cfgParser = JsonConvert.DeserializeObject<JSON>(string.Join("\r\n", cfgLines));
        }

        /// <summary>
        /// Add Asprova object to this Asprova table.
        /// </summary>
        /// <param name="child">Represent a row in the table in most cases.</param>
        /// <returns>The same child object.</returns>
        public AsObject AddChild(AsObject child)
        {
            child.table = this;
            child.asprova = asprova;

            // Get the primary keys to use as an unique key in AsTable.
            var primaryKeys = child.GetValues(true, true); 

            // Use this object to this table.
            Add(string.Join("-", primaryKeys.Where(c => c != null).ToArray()), child);

            // Store first primary keys. Later it will be used at db data download.
            if (!firstPrimaryKeyValues.Contains(primaryKeys[0].ToString())) firstPrimaryKeyValues.Add(primaryKeys[0].ToString());

            return child;
        }

        #region "database"

        public virtual void Save()
        {
            // Check if already saved then exit.
            if (Saved) return;
            Saved = true;

            foreach (AsObject obj in Values)
            {
                obj.Save(target);
            }

            UploadTargetData();

            Clear();

            GC.Collect();
        }


        /// <summary>
        /// Download only schema from the specified table.
        /// </summary>
        public void DownloadTargetDataSchema()
        {
            DownloadTargetData(true);
        }


        /// <summary>
        /// Download data from the specified table.
        /// </summary>
        /// <param name="SchemaOnly"></param>
        public void DownloadTargetData(bool SchemaOnly = false)
        {
            string select = string.Format(@"SELECT * FROM {0}", databaseTableName);
            if (fieldStatusFlag != null && !SchemaOnly && firstPrimaryKeyValues.Count > 0)
            {
                // Download all data where the first primary key is a match, and any other which is not deleted (because if it is not active, than need to flag it as deleted)
                select += string.Format($" WHERE {primaryKeys[0]} IN ('{string.Join("','", firstPrimaryKeyValues)}') OR ({fieldStatusFlag} is null OR {fieldStatusFlag} <> 'D')");
            }

            //Create new connection (opening a new MS SQL connection)
            string[] cfgLines;
            //cfgLines = File.ReadAllLines(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "configs/asprova.json"), Encoding.UTF8);
            //JSON cfgParser = JsonConvert.DeserializeObject<JSON>(string.Join("\r\n", cfgLines));
            SqlConnection actualConn = new SqlConnection(cfgParser.ConnectionStringMSSQL);
            try
            {
                using (actualConn)
                {
                    actualConn.Open();
                    // Create table schema in C# and download records
                    adapterTarget = new SqlDataAdapter(string.Format(select, databaseTableName), actualConn);
                    if (SchemaOnly)
                    {
                        // Download only schema
                        adapterTarget.FillSchema(target, SchemaType.Source);

                        // Collect primary keyss
                        foreach (DataColumn col in target.PrimaryKey)
                        {
                            primaryKeys.Add(col.ColumnName);
                        }

                        // Collect columns
                        foreach (DataColumn col in target.Columns)
                        {
                            columns.Add(col.ColumnName);
                        }

                        return;
                    }

                    // Download all data
                    adapterTarget.Fill(target);

                    // Add virtual column for track changes
                    DataColumn column = new DataColumn(fieldTrackChanges, typeof(sbyte))
                    {
                        AllowDBNull = true
                    };
                    target.Columns.Add(column);
                }
            }
            catch(SqlException ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                actualConn.Close();
            }
        }


        private void UploadTargetData()
        {
            SqlConnection actualConn = new SqlConnection(cfgParser.ConnectionStringMSSQL);
            try
            {
                using (actualConn)
                {
                    actualConn.Open();
                    DataRow[] rows = null;
                    var select = $"{fieldTrackChanges} is null" + (fieldRevision == null ? $" AND({fieldStatusFlag} is null OR {fieldStatusFlag} <> 'D')" : "");
                    rows = target.Select(select);
                    string selectAdapter = string.Format(@"SELECT * FROM {0}", databaseTableName);
                    adapterTarget = new SqlDataAdapter(string.Format(selectAdapter, databaseTableName), actualConn);

                    foreach (var row in rows)
                    {
                        row.BeginEdit();

                        if (fieldRevision == null)
                        {
                            // Mark rows if deleted
                            if (!NoDeleteFlag)
                                row[fieldStatusFlag] = 'D';
                        }
                        else if (fieldValidTo != null && row[fieldValidTo] == DBNull.Value)
                        {
                            // Not live anymore set 
                            row[fieldValidTo] = DateTime.Now;
                        }

                        row.EndEdit();
                    }

                    // Remove helper column
                    target.Columns.Remove(fieldTrackChanges);

                    // UPLOAD
                    var cb = new SqlCommandBuilder(adapterTarget);
                    cb.ConflictOption = ConflictOption.CompareRowVersion;
                    adapterTarget.Update(target);

                    target.Clear();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                actualConn.Close();
                GC.Collect();
            }
        }

        #endregion

        #region "mapping"
        private Dictionary<Type, Dictionary<PropertyInfo, DbAttribute>> mappings = new Dictionary<Type, Dictionary<PropertyInfo, DbAttribute>>();
        private Dictionary<Type, Dictionary<PropertyInfo, DbAttribute>> mappingsRecursive = new Dictionary<Type, Dictionary<PropertyInfo, DbAttribute>>();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        private void LoadMapping(Type type)
        {
            mappings.Add(type, new Dictionary<PropertyInfo, DbAttribute>());

            // Get property array
            var properties = type.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.FlattenHierarchy);

            foreach (var p in properties)
            {
                DbAttribute attr = (DbAttribute)p.GetCustomAttribute(typeof(DbAttribute));
                if (attr == null) continue;
                mappings[type].Add(p, attr);
            }
        }

        public Dictionary<PropertyInfo, DbAttribute> GetMapping(Type type)
        {
            // Load the mapping if not loaded yet
            if (!mappings.ContainsKey(type))
            {
                LoadMapping(type);
            }

            return mappings[type];
        }

        public Dictionary<PropertyInfo, DbAttribute> GetMapping(AsObject obj)
        {
            var type = obj.GetType();

            // Load the mapping if not loaded yet
            if (!mappingsRecursive.ContainsKey(type))
            {
                var r = GetMapping(type);

                if (obj.parent != null)
                {
                    var m = GetMapping(obj.parent);
                    r = r.Union(m).ToDictionary(k => k.Key, v => v.Value);
                }

                mappingsRecursive.Add(type, r);
            }

            return mappingsRecursive[type];
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int SetRevisions()
        {
            // Calculate revision for ALL object in AsTable
            //return 1;

            // target -> tartalmazza a db állapotot
            // Values -> tartalmazza a mostani állapotot


            var pkTarget = new Dictionary<string, List<string>>();
            var pkValues = new Dictionary<string, List<string>>();

            var referenceAndRevision = new Dictionary<string, int>();

            if (fieldReference != null)
            {
                // Values
                foreach (AsObject obj in Values)
                {
                    // Get deepest object
                    var objs = obj.GetDeepestObjects();

                    foreach (var o in objs)
                    {
                        var oPkVals = o.GetValuesDictionary(true, false);

                        string primaryPk = oPkVals[fieldReference].ToString();

                        if (!pkValues.ContainsKey(primaryPk)) pkValues.Add(primaryPk, new List<string>());

                        var recordPks = new List<string>();
                        foreach (var oPk in oPkVals)
                        {
                            if (oPk.Key == fieldRevision && !referenceAndRevision.ContainsKey(oPkVals[fieldReference].ToString()))
                            {
                                referenceAndRevision.Add(oPkVals[fieldReference].ToString(), Convert.ToInt32(oPk.Value));
                                continue;
                            }

                            recordPks.Add(oPk.Value != null ? oPk.Value.ToString() : "");
                        }

                        pkValues[primaryPk].Add(string.Join("|", recordPks));
                    }
                }

                //Db
                foreach (DataRow r in target.Rows)
                {
                    // Skip if row deleted
                    if (r[fieldStatusFlag] != DBNull.Value && r[fieldStatusFlag].ToString() == "D") continue;

                    string primaryPk = r[fieldReference].ToString();

                    if (!pkTarget.ContainsKey(primaryPk)) pkTarget.Add(primaryPk, new List<string>());

                    var recordPks = new List<string>();
                    foreach (var pk in primaryKeys)
                    {
                        if (pk == fieldRevision && !referenceAndRevision.ContainsKey(r[fieldReference].ToString()))
                        {
                            referenceAndRevision.Add(r[fieldReference].ToString(), Convert.ToInt32(r[pk]));
                            continue;
                        }

                        recordPks.Add(r[pk].ToString());
                    }

                    pkTarget[primaryPk].Add(string.Join("|", recordPks));
                }

                var refkeys = pkTarget.Keys.Union(pkValues.Keys).ToList(); // Merge pkTarget and pkValues to a List

                var dictRevisions = new Dictionary<string, int>();

                foreach (var r in refkeys)
                {
                    if (!pkValues.ContainsKey(r))
                    {
                        // Invalidate lejáratni!
                    }
                    else if (!pkTarget.ContainsKey(r))
                    {
                        // Add new item
                        dictRevisions[r] = 1;
                    }
                    else if (pkValues[r].Count != pkTarget[r].Count)
                    {
                        // Reference key record mismatch
                        // Revision up!
                        dictRevisions[r] = referenceAndRevision[r] + 1;
                    }

                    pkValues[r].Sort();
                    pkTarget[r].Sort();

                    for (int i = 0; i < pkValues[r].Count; i++)
                    {
                        var x12 = pkTarget[r][i];
                        var y12 = pkValues[r][i];

                        if (pkValues[r][i] != pkTarget[r][i])
                        {
                            // Mismatch, revision up!
                            //dictRevisions[r] = ++;
                            dictRevisions[r] = referenceAndRevision[r] + 1;
                        }
                    }



                    // no revision change!
                    ////dicPrimaryKeys[r] = ==;
                }

                foreach (var obj in Values)
                {
                    var objs = obj.GetDeepestObjects();

                    foreach (var o in objs)
                    {
                        var oPkVals = o.GetValuesDictionary(true, false);

                        string primaryPk = oPkVals[fieldReference].ToString();

                        obj.Revision = dictRevisions[primaryPk];
                    }
                    //obj.Save(target);
                }
            }

            // TODO: GetCurrentRevision
            if (fieldRevision != null)
            {
                //Console.WriteLine(row[table.fieldRevision]);
                return 1;
                //return Convert.ToInt32(row[table.fieldRevision]);
            }

            return 1;
        }

    }
}
