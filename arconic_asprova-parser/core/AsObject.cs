﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;

namespace graphIT.Asprova
{
    public abstract class AsObject
    {
        public AsBase asprova = null;
        public AsObject parent = null;
        public AsTable table = null;
        public DataRow row = null;
        public List<AsObject> children = new List<AsObject>();
        public List<AsObject> childrenRecursive = new List<AsObject>();

        public int? Revision { get; set; } = null;

        #region "Database fields"
        [Db("error")]
        public string Errors
        {
            get { return GetEWD(EWD.Error); }
        }

        [Db("warning")]
        public string Warnings
        {
            get { return GetEWD(EWD.Warning); }
        }

        [Db("debug")]
        public string Debugs
        {
            get { return GetEWD(EWD.Debug); }
        }
        #endregion

        #region "Error/Warning/Debug"
        public Dictionary<string, List<string[]>> errors = new Dictionary<string, List<string[]>>();
        public Dictionary<string, List<string[]>> warnings = new Dictionary<string, List<string[]>>();
        public Dictionary<string, List<string[]>> debugs = new Dictionary<string, List<string[]>>();

        private void AddEWD(Dictionary<string, List<string[]>> dic, string type, string[] args)
        {
            if (!dic.ContainsKey(type)) dic.Add(type, new List<string[]>());

            if (!dic[type].Contains(args))
            {
                dic[type].Add(args);
            }
        }

        public void AddError(string type, string[] args = null)
        {
            AddEWD(errors, type, args);
        }

        public void AddWarning(string type, string[] args = null)
        {
            AddEWD(warnings, type, args);
        }

        public void AddDebug(string type, string[] args = null)
        {
            AddEWD(debugs, type, args);
        }

        /// <summary>
        /// Get errors, warnings or debugs
        /// </summary>
        /// <param name="type">The type what we would like to get b</param>
        /// <returns></returns>
        private string GetEWD(EWD type)
        {
            Dictionary<string, List<string[]>> dic = null;

            switch (type)
            {
                case EWD.Error:
                    dic = errors;
                    break;

                case EWD.Warning:
                    dic = warnings;
                    break;

                case EWD.Debug:
                    dic = debugs;
                    break;

                default:
                    return null;
            }

            if (dic.Count == 0) return null;
            List<string> res = new List<string>();

            foreach (var s in dic)
            {
                List<string> res2 = new List<string>();
                foreach (var s2 in s.Value)
                {
                    if (s2 == null) continue;
                    res2.Add(string.Join(",", s2));
                }

                if (res2.Count > 0)
                {
                    res.Add($"{s.Key} [{string.Join("|", res2.Distinct())}]");
                }
                else
                {
                    res.Add($"{s.Key}");
                }
            }

            if (parent != null)
            {
                string par = parent.GetEWD(type);
                if (par != null) res.Insert(0, par);
            }

            if (res != null)
            {
                //Debug.WriteLine(string.Join(";", res));
            }

            return string.Join(";", res);
        }
        #endregion

        #region "Primary key"
        private Dictionary<string, object> dictPrimaryKeys = null;
        private Dictionary<string, object> dictValues = null;
        private object[] values = null;
        private object[] primaryKeys = null;


        public object[] GetValues(bool onlyPk, bool forced, bool withRevision = false)
        {
            if (forced || primaryKeys == null || values == null)
            {
                values = new object[] { };
                primaryKeys = new object[] { };

                dictPrimaryKeys = new Dictionary<string, object>();
                dictValues = new Dictionary<string, object>();

                // Collect the primary keys from the AsTable
                foreach (var pk in table.primaryKeys)
                {
                    dictPrimaryKeys.Add(pk, null);
                }

                // Collect all columns from the AsTable
                foreach (var col in table.columns)
                {
                    dictValues.Add(col, null);
                }

                if (parent != null)
                {
                    var dictParent = parent.GetValuesDictionary(false, forced);
                    foreach (var p in dictParent)
                    {
                        if (p.Value == null) continue;
                        dictValues[p.Key] = p.Value;

                        if (dictPrimaryKeys.ContainsKey(p.Key))
                        {
                            dictPrimaryKeys[p.Key] = p.Value;
                        }
                    }
                }

                var mapping = table.GetMapping(GetType());
                foreach (var pair in mapping)
                {
                    if (table.columns.Contains(pair.Value.Field))
                    {
                        object val = pair.Key.GetValue(this);

                        if (val != null)
                        {
                            var t = val.GetType();

                            if (val.GetType().BaseType.IsGenericType)
                            {
                                if (val.GetType().BaseType.GetGenericTypeDefinition() == typeof(AsConst.MyEnum<>))
                                {
                                    val = ((AsConst.MyEnum)val).Val;
                                }
                            }
                        }

                        if (dictValues[pair.Value.Field] == null)
                        {
                            dictValues[pair.Value.Field] = val;
                        }
                        else if (val != null)
                        {
                            dictValues[pair.Value.Field] = $"{dictValues[pair.Value.Field].ToString()};{val.ToString()}";
                        }

                        if (table.primaryKeys.Contains(pair.Value.Field))
                        {
                            dictPrimaryKeys[pair.Value.Field] = dictValues[pair.Value.Field];
                        }
                    }
                }

                // Revisioning
                if (withRevision && table.fieldRevision != null && (children == null || children.Count == 0))
                {
                    dictPrimaryKeys[table.fieldRevision] = table.SetRevisions();
                    dictValues[table.fieldRevision] = table.SetRevisions();
                }

                primaryKeys = dictPrimaryKeys.Values.ToArray();
                values = dictValues.Values.ToArray();
            }

            return onlyPk ? primaryKeys : values;
        }

        public Dictionary<string, object> GetValuesDictionary(bool onlyPk, bool forced, bool withRevision = false)
        {
            GetValues(onlyPk, forced, withRevision);

            return onlyPk ? dictPrimaryKeys : dictValues;
        }
        #endregion

        /// <summary>
        /// Record order to database.
        /// </summary>
        /// <param name="dt">Target table.</param>
        public virtual void Save(DataTable dt)
        {
            var objs = GetDeepestObjects();

            foreach (var obj in objs)
            {
                obj.SaveInternal(dt);
            }
        }

        /// <summary>
        /// Record object to database.
        /// </summary>
        /// <param name="dt">Target table.</param>
        private void SaveInternal(DataTable dt)
        {
            StatusFlag status = StatusFlag.None;

            

            // DataRow not yet assigned
            if (row == null)
            {
                // Try to find it
                row = dt.Rows.Find(GetValues(true, true));
                if (row == null)
                {
                    // Row not exists yet, create it
                    status = StatusFlag.Added;
                    row = dt.NewRow();
                }
            }

            row.BeginEdit();

            // Update values
            Type type = GetType();
            var mapping = table.GetMapping(this);

            var vals = GetValuesDictionary(false, true);

            foreach (var pair in mapping.Values)
            {
                SetRowValue(row, pair.Field, vals[pair.Field], status);
            }

            // Set changed flag
            SetChangeFlag(status);

            if (table.fieldReference != null)
            {
                row[table.fieldRevision] = Revision;
            }
          
            row.EndEdit();

            // If new row, then add it to table
            if (status == StatusFlag.Added)
                try
                {
                    dt.Rows.Add(row);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
        }

        public List<AsObject> GetDeepestObjects()
        {
            childrenRecursive.Clear();

            GetDeepestObjects(this);

            return childrenRecursive;
        }

        /// <summary>
        /// Get the deepest object of the current object
        /// </summary>
        /// <param name="current">Current object</param>
        private void GetDeepestObjects(AsObject current)
        {
            if (current.children != null && current.children.Count > 0)
            {
                foreach (var child in current.children)
                {
                    GetDeepestObjects(child);
                }
            }
            else
            {
                // Deepest (no children).
                childrenRecursive.Add(current);
            }
        }

        /// <summary>
        /// Set status flag
        /// </summary>
        /// <param name="status"></param>
        private void SetChangeFlag(StatusFlag status)
        {
            // Set that the row is exists.
            row[table.fieldTrackChanges] = 1;

            if (status == StatusFlag.Added || row[table.fieldStatusFlag].ToString() == "D")
            {
                row[table.fieldStatusFlag] = "A";
                row[table.fieldLastUpdate] = DateTime.Now;
                if (status == StatusFlag.Added && table.fieldCreatedAt != null) row[table.fieldCreatedAt] = DateTime.Now;
            }
            else if (status == StatusFlag.Modified && (row[table.fieldStatusFlag] == DBNull.Value || ((string)row[table.fieldStatusFlag]) != "A"))
            {
                // Only set to M when the status is not A
                row[table.fieldStatusFlag] = "M";
            }
        }

        /// <summary>
        /// Set a new value for the current row and change the status flag
        /// </summary>
        /// <param name="row">The current row</param>
        /// <param name="field">The field name of the value</param>
        /// <param name="value">The new value what we will set for the row</param>
        /// <param name="status">The current status flag of the row</param>
        private static void SetRowValue(DataRow row, string field, object value, StatusFlag status = StatusFlag.None)
        {
            value = value ?? DBNull.Value;
            if (!row[field].Equals(value))
            {
                row[field] = value;
                if (status == StatusFlag.None)
                {
                    status = StatusFlag.Modified;
                }
            }
        }

        #region Enums

        private enum EWD
        {
            Error,
            Warning,
            Debug
        }

        private enum StatusFlag
        {
            None,
            Added,
            Modified,
            Deleted
        }

        #endregion
    }
}
