﻿using System.Diagnostics;

namespace graphIT.Asprova
{
    public static partial class AsConst
    {
        public abstract class MyEnum
        {
            public object Val { get; set; }
        }

        [DebuggerDisplay("{DebuggerDisplay,nq}")]
        public abstract class MyEnum<T> : MyEnum
        {
            public MyEnum(T value, string name)
            {
                Name = name;
                Value = value;
                Val = value;
            }

            public string Name { get; set; }

            public T Value { get; set; }

            public override bool Equals(object obj)
            {
                return obj is MyEnum<T> && this == (MyEnum<T>)obj;
            }

            public override int GetHashCode()
            {
                return Val.GetHashCode();
            }

            public static bool operator ==(MyEnum<T> x, MyEnum<T> y)
            {
                return (x is null && y is null) || (!(x is null) && !(y is null) && Equals(x.Val, y.Val));
            }
            public static bool operator !=(MyEnum<T> x, MyEnum<T> y)
            {
                return (!(x == y));
            }

            [DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private string DebuggerDisplay
            {
                get { return Name; }
            }
        }

        public static partial class Item
        {
            public partial class ItemType : MyEnum<string>
            {
                // 0,M,Raw material,,;1,U,Purchased part,,;2,H,Manufactured part,,;3,I,Intermediate item,,;4,P,Finished item,,
                private ItemType(string value, string name) : base(value, name) { }

                public static ItemType RawMaterial { get { return new ItemType("M", "RawMaterial"); } }
                public static ItemType Purchased { get { return new ItemType("U", "Purchased"); } }
                public static ItemType Produced { get { return new ItemType("H", "Produced"); } }
                public static ItemType Intermediate { get { return new ItemType("I", "Intermediate"); } }
                public static ItemType Final { get { return new ItemType("P", "Final"); } }
            }

            public partial class Replenishment : MyEnum<int>
            {
                // 0,0,No,,;1,1,Yes,,;2,2,Yes (one-to-one production),,;3,3,Yes (inventory + one-to-one production),,;4,4,Yes (supply-demand adjustment one-to-one production),,
                private Replenishment(int value, string name) : base(value, name) { }

                public static Replenishment No { get { return new Replenishment(0, "No"); } }
                public static Replenishment Yes { get { return new Replenishment(1, "Yes"); } }
                public static Replenishment YesOneToOneProduction { get { return new Replenishment(2, "Yes OneToOneProduction"); } }
                public static Replenishment YesInventoryAndOneToOneProduction { get { return new Replenishment(3, "Yes Inventory And OneToOneProduction"); } }
                public static Replenishment YesSupplyDemandAdjustmentOneToOneProduction { get { return new Replenishment(4, "Yes SupplyDemandAdjustment OneToOneProduction"); } }
            }

            public partial class ObtainMethod : MyEnum<string>
            {
                // 0,N,Not allowed,,;1,M,Produce,,;2,P,Purchase,,;3,IM,Prefer to produce ,,;4,IP,Prefer to purchase,,
                private ObtainMethod(string value, string name) : base(value, name) { }

                public static ObtainMethod NotAllowed { get { return new ObtainMethod("N", "Mixed"); } }
                public static ObtainMethod Produce { get { return new ObtainMethod("M", "Produce"); } }
                public static ObtainMethod Purchase { get { return new ObtainMethod("P", "Purchase"); } }
                public static ObtainMethod PreferToProduce { get { return new ObtainMethod("IM", "Prefer to produce"); } }
                public static ObtainMethod PreferToPurchase { get { return new ObtainMethod("IP", "Prefer to purchase"); } }
            }

            public partial class MaterialSwitchType : MyEnum<string>
            {
                // 0,M,Mixed,,;1,O,Non-mixed,,
                private MaterialSwitchType(string value, string name) : base(value, name) { }

                public static MaterialSwitchType Mixed { get { return new MaterialSwitchType("M", "Mixed"); } }
                public static MaterialSwitchType NonMixed { get { return new MaterialSwitchType("O", "Non mixed"); } }
            }
        }

        public static partial class Order
        {
            public partial class AssignmentDirection : MyEnum<string>
            {
                // 0,,Unspecified,,;1,F,Forward,,;2,B,Backward,,;3,P,According to order priority,,
                private AssignmentDirection(string value, string name) : base(value, name) { }

                public static AssignmentDirection Unspecified { get { return new AssignmentDirection(null, "Unspecified"); } }
                public static AssignmentDirection Forward { get { return new AssignmentDirection("F", "Forward"); } }
                public static AssignmentDirection Backward { get { return new AssignmentDirection("B", "Backward"); } }
                public static AssignmentDirection AccordingToOrderPriority { get { return new AssignmentDirection("P", "According to order priority"); } }
            }

            public partial class OrderType : MyEnum<string>
            {
                // S-Sales order;M-Manufacturing order;P-Purchase order;I-Inventory (absolute);J-Inventory (fluctuation)
                private OrderType(string value, string name) : base(value, name) { }

                public static OrderType Sales { get { return new OrderType("S", "Sales"); } }
                public static OrderType Manufacturing { get { return new OrderType("M", "Manufacturing"); } }
                public static OrderType Purchase { get { return new OrderType("P", "Purchase"); } }
                public static OrderType InventoryAbs { get { return new OrderType("I", "InventoryAbs"); } }
                public static OrderType InventoryRel { get { return new OrderType("J", "InventoryRel"); } }
            }

            public partial class OrderClass : MyEnum<int>
            {
                // 0,0,Registered order,,;1,1,Replenishment order,,
                private OrderClass(int value, string name) : base(value, name) { }

                public static OrderClass Registered { get { return new OrderClass(0, "Registered"); } }
                public static OrderClass Replenishment { get { return new OrderClass(1, "Replenishment"); } }
            }

            public partial class Direction : MyEnum<string>
            {
                private Direction(string value, string name) : base(value, name) {}

                public static Direction Forward { get { return new Direction("F", "Forward"); } }
                public static Direction Backward { get { return new Direction("B", "Backward"); } }
            }
        }

        public static partial class Master
        {
            public partial class TimeConstraintMethod : MyEnum<string>
            {
                // 0,ES,,,;1,SS,,,;2,SSEE,,,;3,ESE,,,;4,EES,,,;6,SSEEE,,,;7,ESSEE,,,;8,GES,,,
                private TimeConstraintMethod(string value, string name) : base(value, name) { }

                public static TimeConstraintMethod ES { get { return new TimeConstraintMethod("ES", "ES"); } }
                public static TimeConstraintMethod SS { get { return new TimeConstraintMethod("SS", "SS"); } }
                public static TimeConstraintMethod SSEE { get { return new TimeConstraintMethod("SSEE", "SSEE"); } }
                public static TimeConstraintMethod ESE { get { return new TimeConstraintMethod("ESE", "ESE"); } }
                public static TimeConstraintMethod EES { get { return new TimeConstraintMethod("EES", "EES"); } }
                public static TimeConstraintMethod SSEEE { get { return new TimeConstraintMethod("SSEEE", "SSEEE"); } }
                public static TimeConstraintMethod ESSEE { get { return new TimeConstraintMethod("ESSEE", "ESSEE"); } }
                public static TimeConstraintMethod GES { get { return new TimeConstraintMethod("GES", "GES"); } }
            }
        }

        public static partial class Operation
        {
            public partial class Status : MyEnum<string>
            {
                // 0,,Unspecified,,;1,N,Unscheduled,,;2,A,Scheduled,,;3,I,Released,,;4,D,Frozen ,,;5,T,Started,,;6,B,Completed,,
                private Status(string value, string name) : base(value, name) { }

                public static Status Unscheduled { get { return new Status("N", "Unscheduled"); } }
                public static Status Scheduled { get { return new Status("A", "Scheduled"); } }
                public static Status Released { get { return new Status("I", "Released"); } }
                public static Status Frozen { get { return new Status("D", "Frozen"); } }
                public static Status Started { get { return new Status("T", "Started"); } }
                public static Status Completed { get { return new Status("B", "Completed"); } }
            }

            public partial class Type : MyEnum<string>
            {
                // 0,O,Operation,,;1,S,Split operation root,,;2,P,Production task,,;3,E,Setup task,,;4,T,Teardown task,,;5,R,Resource lock task,,;6,C,Changeover task,,;8,L,,,;9,G,Operation group,,
                private Type(string value, string name) : base(value, name) { }

                public static Type Operation { get { return new Type("O", "Operation"); } }
                public static Type SplitOperationRoot { get { return new Type("S", "Split operation root"); } }
                public static Type ProductionTask { get { return new Type("P", "Production task"); } }
                public static Type SetupTask { get { return new Type("E", "Setup task"); } }
                public static Type TeardownTask { get { return new Type("T", "Teardown task"); } }
                public static Type ResourceLockTask { get { return new Type("R", "Resource lock task"); } }
                public static Type ChangeoverTask { get { return new Type("C", "Changeover task"); } }
                public static Type OperationGroup { get { return new Type("G", "Operation group"); } }
            }
        }

        public static partial class Result
        {
            public partial class TaskIndex : MyEnum<int>
            {
                // 0,,Unspecified,,;1,N,Unscheduled,,;2,A,Scheduled,,;3,I,Released,,;4,D,Frozen ,,;5,T,Started,,;6,B,Completed,,
                private TaskIndex(int value, string name) : base(value, name) { }

                public static TaskIndex Setup { get { return new TaskIndex(1, "Setup"); } }
                public static TaskIndex Production { get { return new TaskIndex(2, "Production"); } }
                public static TaskIndex Teardown { get { return new TaskIndex(3, "Teardown"); } }
            }
        }

        public static partial class Resource
        {
            public partial class ConstrainResQty : MyEnum<string>
            {
                // 0,R,Constrained,,;1,N,Not constrained,,;2,V,According to resource quantity,,
                private ConstrainResQty(string value, string name) : base(value, name) { }

                public static ConstrainResQty Constrained { get { return new ConstrainResQty("R", "Constrained"); } }
                public static ConstrainResQty NotConstrained { get { return new ConstrainResQty("N", "Not constrained"); } }
                public static ConstrainResQty AccordingToResourceQuantity { get { return new ConstrainResQty("V", "According to resource quantity"); } }
            }
        }
    }
}
