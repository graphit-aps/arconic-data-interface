﻿using CommandLineParser.Arguments;
using graphIT.Arconic.AsprovaParser;
using Arconic.DataUploader;
using Arconic;
using graphIT.Arconic.TestConnection;
using Newtonsoft.Json;
using Org.Mentalis.Files;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Arconic.DataInterface
{
    class DataInterface
    {
        static void Main(string[] args)
        {
            parsedArgs p = parseCmdArgs(args);

            // No other instance running? kilép, ha egyszerre fut többször, és nincs engedélyezve a párhuzamos futtatás
            if (!p.paralel && AlreadyRunning()) return;

            //Console.OutputEncoding = Encoding.UTF8;
            string[] tasks;
            string iniPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + ".ini");
            IniReader iniFile = new IniReader(iniPath);

            // Command argumentum separated by | --> different tasks
            tasks = p.tasks.Split('|');
            string connectionString = "";
            connectionString = p.connectionString;

            foreach (string task in tasks)
            {                
                // When using debugger, no error handling
                if (Debugger.IsAttached)
                {
                    ProcessTask(ref iniFile, task, connectionString);
                }
                else
                {
                    try
                    {
                        ProcessTask(ref iniFile, task, connectionString);
                    } catch (Exception ex)
                    {
                        // Error handling, rollback any changes then throw error to upper level
                        printErrorToIni(ex, iniFile, task);
                    }
                }

                Console.WriteLine(" End: " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                Console.WriteLine("_________________________");
            }

            iniFile = null;

            Console.WriteLine("Press any key to close...");
            Console.ReadKey();
        }

        static void ProcessTask(ref IniReader iniFile, string task, string sqlConnection = null)
        {
            Console.WriteLine("Task: " + task);
            Console.WriteLine(" Start: " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            Console.WriteLine("");

            // Update ini file
            iniFile.Write("Status", task, "running");
            iniFile.Write("Timestamp", task, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            iniFile.DeleteKey("Error", task);
            iniFile.DeleteKey("ErrorDetails", task);
            iniFile.DeleteKey("ErrorStackTrace", task);

            string[] cfgLines;

            switch (task)
            {
                case "asprova":
                    cfgLines = File.ReadAllLines(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "configs/asprova.json"), Encoding.UTF8);
                    JSON cfgParser = JsonConvert.DeserializeObject<JSON>(string.Join("\r\n", cfgLines));
                    AsprovaParser procAsprova = new AsprovaParser(cfgParser);
                    break;

                case "upload": // Itt most a kapcsolódási teszt szerepel benne
                    DataUploader.DataUploader uploader = new DataUploader.DataUploader();
                    break;

                case "testconnection":                   
                    TestConnection testuploader = new TestConnection(sqlConnection);
                    break;
                    
                default:
                    Console.WriteLine(task + " unkown");
                    return;
            }

            Console.WriteLine(" Successful");
            iniFile.Write("Status", task, "success");
        }

        private static parsedArgs parseCmdArgs(string[] args)
        {
            /* Convert from UTF8 is necessary */
            if (ContainsUnicodeCharacter(String.Join(" ", args)))
            {
                for (int i = 0; i < args.Length; i++)
                {
                    byte[] argBytes = Encoding.Default.GetBytes(args[i]);
                    args[i] = Encoding.UTF8.GetString(argBytes);
                }
            }

            /* CommandLineParser */
            CommandLineParser.CommandLineParser parser = new CommandLineParser.CommandLineParser();
            parsedArgs p = new parsedArgs();
            // read the argument attributes from the class definition
            parser.ExtractArgumentAttributes(p);

            /* parse the command line */
            parser.ParseCommandLine(args);

            return p;
        }

        private static bool ContainsUnicodeCharacter(string input)
        {
            const int MaxAnsiCode = 255;

            return input.Any(c => c > MaxAnsiCode);
        }

        /* arguments are defined using attributes, parsed values will be mapped to fields or properties*/
        // Ez a megadás azt jelenti, hogy a parancsot úgy adod meg, hogy -t "és itt jön a task"
        private class parsedArgs
        {
            /* SwitchArguments - for on/off boolean logic */
            [ValueArgument(typeof(string), 't', "tasks", Optional = false, Description = "Define the task(s)")]
            public string tasks = "";

            /* SwitchArguments - for on/off boolean logic */
            [ValueArgument(typeof(string), 'c', "connectionString", Optional = true, Description = "ConnectionString")]
            public string connectionString = "";

            /* SwitchArguments - for on/off boolean logic */
            [ValueArgument(typeof(bool), 'p', "parallel", Optional = true, Description = "Can run paralel")]
            public bool paralel = false;
        }

        private static void printErrorToIni(Exception ex, IniReader iniFile, string task)
        {
            Console.WriteLine(" Hiba");
            iniFile.Write("Status", task, "error");

            if (ex.Message != null)
            {
                iniFile.Write("Error", task, "\"" + Regex.Replace(ex.Message, "[?{}|&~![()^\"]+", "").Replace("\r", "\\ NEWLINE \\").Replace("\n", "") + "\"");
                Console.WriteLine("  *" + ex.Message);
            }

            if (ex.InnerException != null)
            {
                iniFile.Write("ErrorDetails", task, "\"" + Regex.Replace(ex.InnerException.ToString(), "[?{}|&~![()^\"]+", "").Replace("\r", "\\ NEWLINE \\").Replace("\n", "") + "\"");
                Console.WriteLine("  *" + ex.InnerException.ToString());
            }

            if (ex.StackTrace != null)
            {
                iniFile.Write("ErrorStackTrace", task, "\"" + Regex.Replace(ex.StackTrace.ToString(), "[?{}|&~![()^\"]+", "").Replace("\r", "\\ NEWLINE \\").Replace("\n", "") + "\"");
                Console.WriteLine("  *" + ex.StackTrace.ToString());
            }
        }

        private static bool AlreadyRunning()
        {
            Process[] processes = Process.GetProcesses();
            Process currentProc = Process.GetCurrentProcess();

            string location = System.Reflection.Assembly.GetExecutingAssembly().Location;

            foreach (Process process in processes)
            {
                try
                {
                    if (process.Modules[0].FileName == location
                                && currentProc.Id != process.Id)
                        return true;
                } catch { }
            }

            return false;
        }
    }
}
